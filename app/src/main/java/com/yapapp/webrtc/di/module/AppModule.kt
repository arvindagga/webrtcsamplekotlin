package com.yapapp.webrtc.di.module


import android.content.Context
import android.content.SharedPreferences
import android.media.AudioManager
import android.os.CountDownTimer
import android.preference.PreferenceManager
import com.dizzipay.railsbank.data.remote.ApiConstants
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.rontaxi.retrofit.ApiService
import com.rontaxi.retrofit.TaxiInterceptor
import com.yapapp.webrtc.R
import com.yapapp.webrtc.WebRTCApp
import com.yapapp.webrtc.call.CallManager
import com.yapapp.webrtc.call.callsockets.CallSocketEventsManager
import com.yapapp.webrtc.call.rtc.RTCClient
import com.yapapp.webrtc.call.timer.TimerManager
import com.yapapp.webrtc.constants.SocketConstants
import com.yapapp.webrtc.model.signup.SignUpRepo
import com.yapapp.webrtc.sockets.SocketManager
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import org.webrtc.EglBase
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

//@Module(includes = ViewModelModule.class)
//         class AppModule() {
//
//        }


@Module
class AppModule() {


    // --- REPOSITORY INJECTION ---
    @Provides
    @Singleton
    fun provideSignUpRepo(apiService: ApiService): SignUpRepo {
        return SignUpRepo(apiService)
    }


    @Provides
    fun provideExecutor(): Executor {
        return Executors.newSingleThreadExecutor()
    }

    @Provides
    fun provideSharedPrefrence(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }


    @Provides
    @Singleton
    fun provideGoogleSignInOptions(): GoogleSignInOptions {
        return GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(WebRTCApp.context?.getString(R.string.web_client_id))
            .requestEmail()
            .build()
    }


    @Provides
    @Singleton
    fun provideFirebaseAuth(): FirebaseAuth {
        return FirebaseAuth.getInstance()
    }

    @Provides
    @Singleton
    fun providerInterceptor(context: Context): TaxiInterceptor {
        return TaxiInterceptor(context)
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(interceptor: TaxiInterceptor): OkHttpClient {

        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.connectTimeout(ApiConstants.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
        okHttpClient.readTimeout(ApiConstants.READ_TIMEOUT, TimeUnit.MILLISECONDS)
        okHttpClient.writeTimeout(ApiConstants.WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
//        val logInterceptor = HttpLoggingInterceptor()
//        logInterceptor.level=(HttpLoggingInterceptor.Level.BODY)
        okHttpClient.addInterceptor(interceptor)
        //   okHttpClient.addInterceptor(logInterceptor)

        return okHttpClient.build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): ApiService {

        val retrofit = Retrofit.Builder()
            .baseUrl(ApiConstants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(ApiService::class.java)

    }


    @Singleton
    @Provides
    fun provideSocket() =
        IO.socket(SocketConstants.SOCKET_URL)

    @Singleton
    @Provides
    fun provideSocketManager(socket: Socket, context: Context) =
        SocketManager(socket, context)

    @Singleton
    @Provides
    fun provideRootEglBase() = EglBase.create()


    @Singleton
    @Provides
    fun provideRTCClient(context: Context,eglBase: EglBase) = RTCClient(context,eglBase)


    @Singleton
    @Provides
    fun provideCallSocketManager(socketManager: SocketManager) =
        CallSocketEventsManager(socketManager)

    @Singleton
    @Provides
    fun provideCallManager(
        context: Context,
        rtcClient: RTCClient,
        callSocketEventsManager: CallSocketEventsManager,
        audioManager: com.yapapp.webrtc.call.audio.AudioManager,
        timerManager: TimerManager
    ) = CallManager(context, rtcClient, callSocketEventsManager, audioManager, timerManager).apply {
        this.setObserver()
    }


    @Singleton
    @Provides
    fun provideAudioManager(context: Context): AudioManager{

        return  context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    }

    @Singleton
    @Provides
    fun provideRtcAudioManager(context: Context,audioManager: AudioManager)=
        com.yapapp.webrtc.call.audio.AudioManager(context,audioManager)


    @Singleton
    @Provides
    fun provideTimerManager( ) = TimerManager()

}