package com.yapapp.webrtc.view.home.userlist

import android.view.View
import androidx.lifecycle.Observer
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.yapapp.webrtc.WebRTCApp
import com.yapapp.webrtc.cache.clearAllData
import com.yapapp.webrtc.view.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.ArrayList


fun HomeFragment.observeUserSelection() {
    homeViewModel.userSelection.observe(this, Observer {


        selectedlist = it as ArrayList<String>
        if (it.size > 0) {
            constraintLayout_action.visibility = View.VISIBLE
            (activity as HomeActivity).supportActionBar?.hide()

        } else {
            constraintLayout_action.visibility = View.GONE
            (activity as HomeActivity).supportActionBar?.show()
        }

    })
}

fun HomeFragment.logOut() {

    mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso);

    mGoogleSignInClient.signOut().addOnCompleteListener {
        auth.signOut()

        clearAllData(WebRTCApp.context!!)

        activity!!.onBackPressed()
    }
}

fun HomeFragment.clearAllSelections() {

    var list: ArrayList<String> =
        homeViewModel.userSelection.value as ArrayList<String>
    list.clear()
    homeViewModel.userSelection.postValue(list)
    usersAdapter.notifyDataSetChanged()
}
