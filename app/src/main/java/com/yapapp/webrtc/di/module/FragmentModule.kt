package com.yapapp.webrtc.di.module

import com.yapapp.webrtc.view.call.CallFragment
import com.yapapp.webrtc.view.call.OngoingCallFragment
import com.yapapp.webrtc.view.initial.LoginFragment
import com.yapapp.webrtc.view.home.userlist.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    internal abstract fun loginFragment(): LoginFragment

    @ContributesAndroidInjector
    internal abstract fun homeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun ongoingCallFragment(): OngoingCallFragment

    @ContributesAndroidInjector
    internal abstract fun callFragment(): CallFragment


}