package com.yapapp.webrtc.util

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View


open class AllPurposeTouchListener
/**
 * Constructor
 *
 * @param ctx
 */
    (ctx: Context) : View.OnTouchListener {

    // Constants
    private val mGestureDetector: GestureDetector
    private val mGestureListener: GestureListener

    init {
        mGestureListener = GestureListener()
        mGestureDetector = GestureDetector(ctx, mGestureListener)
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        mGestureListener.setView(v)
        if (event.getActionMasked() == MotionEvent.ACTION_UP || event.getActionMasked() == MotionEvent.ACTION_DOWN) {

            /**
             * when user leaves the touch or ends sliding , put back view to its initial position
             */
            v.setTranslationY(0f);

            v.clearAnimation()
            //  v?.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));


        }
        return mGestureDetector.onTouchEvent(event)
    }

    open inner class GestureListener : GestureDetector.SimpleOnGestureListener() {

        // Constants
        private val SWIPE_THRESHOLD = 100
        private val SWIPE_VELOCITY_THRESHOLD = 100

        private var mView: View? = null

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            return this@AllPurposeTouchListener.onSingleTapConfirmed(mView)
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return this@AllPurposeTouchListener.onSingleTapUp(mView)
        }

        override fun onLongPress(e: MotionEvent) {
            this@AllPurposeTouchListener.onLongPress(mView)
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent?,
            distanceX: Float,
            distanceY: Float
        ): Boolean {

            if (e1 != null && e2 != null) {
                // The difference in the Y position
                val diffY = e2.y - e1.y
                // The difference in the X position
                val diffX = e2.x - e1.x

                mView?.setTranslationY(diffY);


//                if (Math.abs(diffY) > SWIPE_THRESHOLD) {
//
//                    //  mView?.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
//
//
//                    mView?.setTranslationY(SWIPE_THRESHOLD.toFloat());
//                } else {
////                    mView?.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
//
//
//                    mView?.setTranslationY(diffY);
//                }
            }
            return true
        }

        override fun onFling(
            e1: MotionEvent,
            e2: MotionEvent,
            velocityX: Float,
            velocityY: Float
        ): Boolean {

            var result = false

            try {

                // The difference in the Y position
                val diffY = e2.y - e1.y
                // The difference in the X position
                val diffX = e2.x - e1.x

                if (Math.abs(diffX) > Math.abs(diffY)) {
                    // The fling is horizontal
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        // The fling is actually a fling
                        if (diffX > 0) {
                            // The fling is to the right (the difference in the position is positive)
                            onSwipeRight()
                        } else {
                            // The fling is to the left (the difference in the position is negative)
                            onSwipeLeft()
                        }
                        result = true
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    // The fling is vertical and is actually a fling
                    if (diffY > 0) {
                        // The fling is downwards
                        onSwipeBottom()

                    } else {
                        // The fling is upwards
                        onSwipeTop()
                    }
                    result = true
                }

            } catch (exception: Exception) {
                exception.printStackTrace()
            }

            return result

        }

        fun setView(view: View) {
            mView = view
        }


    }

    /**
     * Notified when a single-tap occurs.
     *
     *
     * Unlike [.onSingleTapUp], this
     * will only be called after the detector is confident that the user's
     * first tap is not followed by a second tap leading to a double-tap
     * gesture.
     *
     * @param v The view the tap occurred on.
     */
    fun onSingleTapConfirmed(v: View?): Boolean {
        return false
    }

    /**
     * Notified when a tap occurs.
     *
     * @param v The view the tap occurred on.
     */
    fun onSingleTapUp(v: View?): Boolean {
        return false
    }

    fun onLongPress(v: View?) {}

    /**
     * If the user swipes right
     */
    open fun onSwipeRight() {}

    /**
     * If the user swipes left
     */
    open fun onSwipeLeft() {}

    /**
     * If the user swipes up
     */
    open fun onSwipeTop() {}

    /**
     * Guess what? if the user swipes down
     */
    open fun onSwipeBottom() {}
}