package com.yapapp.webrtc.call

interface CallInterface {
    fun onCallAccepted()
    fun onCallEnded()


    fun onRemoteVideoUpdated(isEnabled: Boolean)
    fun onRemoteAudioUpdated(isEnabled: Boolean)
}