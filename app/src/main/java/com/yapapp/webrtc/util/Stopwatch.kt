package com.yapapp.webrtc.util

import java.util.*


class Stopwatch {

    // Variables
    private var startTime: Long = 0
    private var stopTime: Long = 0
    private var running = false

    /**
     * Elapsed time in milliseconds
     */
    val elapsedTime: Long
        get() = if (running) {
            System.currentTimeMillis() - startTime
        } else stopTime - startTime

    /**
     * Elapsed time in seconds
     */
    val elapsedTimeSecs: Long
        get() {
            return if (running) {
                ((System.currentTimeMillis() - startTime) / 1000)
            } else ((stopTime - startTime) / 1000)
        }

    /**
     * Returns the time in a string format
     */
    val stringTime: String
        get() {
            val currentTime = elapsedTime
            var seconds = currentTime.toInt() / 1000
            var minutes = seconds / 60
            val hours = minutes / 60
            seconds -= minutes * 60
            minutes -= hours * 60
            return String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds)
        }

    /**
     * Starts the timer
     */
    fun start() {
        this.startTime = System.currentTimeMillis()
        this.running = true
    }

    /**
     * Stops the timer
     */
    fun stop() {
        this.stopTime = System.currentTimeMillis()
        this.running = false
    }
}