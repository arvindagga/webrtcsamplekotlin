package com.yapapp.webrtc.util

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity


class Utilities {


    companion object {

        val DEFAULT_DIALER_RC = 11
        val PERMISSION_RC = 10

        // Constants
        val LONG_VIBRATE_LENGTH: Long = 500
        val SHORT_VIBRATE_LENGTH: Long = 20
        val DEFAULT_VIBRATE_LENGTH: Long = 100


        /**
         * Checks for granted permission but by a single string (single permission)
         *
         * @param permission
         * @return boolean
         */
        fun checkPermissionsGranted(context: Context, permission: String): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(
                context, permission
            ) == PackageManager.PERMISSION_GRANTED
        }

        /**
         * Check for permissions by a given list
         *
         * @param permissions
         * @return boolean
         */
        fun checkPermissionsGranted(context: Context, permissions: Array<String>): Boolean {
            for (permission in permissions) {
                if (!checkPermissionsGranted(context, permission)) {
                    return false
                }
            }
            return true
        }

        /**
         * Check for permissions by a given list
         *
         * @param grantResults
         * @return boolean
         */
        fun checkPermissionsGranted(grantResults: IntArray): Boolean {
            for (result in grantResults) {
                if (result == PackageManager.PERMISSION_DENIED)
                    return false
            }
            return true
        }

        fun askForPermission(activity: FragmentActivity, permission: String) {
            askForPermissions(activity, arrayOf(permission))
        }

        /**
         * Asks for permissions by a given list
         *
         * @param activity
         * @param permissions
         */
        fun askForPermissions(activity: FragmentActivity, permissions: Array<String>) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(permissions, PERMISSION_RC)
            }
        }

        /**
         * Vibrate the phone
         *
         * @param millis the amount of milliseconds to vibrate the phone for.
         */
        @JvmOverloads
        fun vibrate(context: Context, millis: Long = DEFAULT_VIBRATE_LENGTH) {
            val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator ?: return
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(
                    VibrationEffect.createOneShot(
                        millis,
                        VibrationEffect.DEFAULT_AMPLITUDE
                    )
                )
            } else {
                vibrator.vibrate(millis)
            }
        }

        /**
         * Get the dpi for this phone
         *
         * @return the dpi
         */
        fun dpi(context: Context): Float {
            val displayMetrics = DisplayMetrics()
            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            return displayMetrics.densityDpi.toFloat()
        }

        /**
         * This method converts dp unit to equivalent pixels, depending on device density.
         *
         * @param context Context to get resources and device specific display metrics
         * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
         * @return A float value to represent px equivalent to dp depending on device density
         */
        fun convertDpToPixel(context: Context, dp: Float): Float {
            return dp * (dpi(context) / DisplayMetrics.DENSITY_DEFAULT)
        }

        /**
         * This method converts device specific pixels to density independent pixels.
         *
         * @param context Context to get resources and device specific display metrics
         * @param px      A value in px (pixels) unit. Which we need to convert into db
         * @return A float value to represent dp equivalent to px value
         */
        fun convertPixelsToDp(context: Context, px: Float): Float {
            return px / (dpi(context) / DisplayMetrics.DENSITY_DEFAULT)
        }


        /**
         * Get whether a given x and y coordinates are in the vicinity of a view
         *
         * @param view                 the target view
         * @param x                    the x value of the point - must be raw
         * @param y                    the y value of the point - must be raw
         * @param buttonVicinityOffset the vicinity in which to check the condition - in dp
         * @return true if the point is in the view's vicinity, false otherwise.
         */
        fun inViewInBounds(view: View, x: Int, y: Int, buttonVicinityOffset: Int): Boolean {
            var outRect = Rect()
            val location = IntArray(2)
            view.getDrawingRect(outRect)
            view.getLocationOnScreen(location)

            outRect.offset(location[0], location[1])

            val e = convertDpToPixel(view.context, buttonVicinityOffset.toFloat()).toInt()
            outRect = Rect(
                outRect.left - e,
                outRect.top - e,
                outRect.right + e,
                outRect.bottom + e
            )
            return outRect.contains(x, y)
        }


        /**
         * Returns whither the device has an active navigation bar
         *
         * @param context
         * @return boolean
         */
        fun hasNavBar(context: Context): Boolean {
            val resources = context.resources
            val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
            return id > 0 && resources.getBoolean(id)
        }

        /**
         * Returns the navigation bar height
         *
         * @param context
         * @return int
         */
        fun navBarHeight(context: Context): Int {
            val resources = context.resources
            val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
            return if (resourceId > 0) {
                resources.getDimensionPixelSize(resourceId)
            } else 0
        }

        /**
         * Toggles the keyboard according to given parameter (boolean)
         *
         * @param context
         * @param view
         * @param isToOpen
         */
        fun toggleKeyboard(context: Context, view: View, isToOpen: Boolean) {
            if (isToOpen) {
                val imm =
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            } else {
                val imm =
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
            }
        }

        /**
         * Checks wither the text given is numeric or not
         *
         * @param text
         * @return true/false
         */
        fun checkNumeric(text: String): Boolean {
            var numeric = true

            try {
                val num = java.lang.Double.parseDouble(text)
            } catch (e: NumberFormatException) {
                numeric = false
            }

            return numeric
        }
    }

}
/**
 * Vibrate the phone for `DEFAULT_VIBRATE_LENGTH` milliseconds
 */