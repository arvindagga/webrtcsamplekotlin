package com.yapapp.webrtc.model

import android.os.Parcel
import android.os.Parcelable

class User() : Parcelable {
    var givenName: String? = null
    var familyName: String? = null
    var displayName: String? = null
    var googleIdToken: String? = null
    var serverAuthCode: String? = null
    var email: String? = null
    var profileImage: String? = null
    var loginToken: String? = null
    var id: String? = null
    var device = Device()

    constructor(parcel: Parcel) : this() {
        givenName = parcel.readString()
        familyName = parcel.readString()
        displayName = parcel.readString()
        googleIdToken = parcel.readString()
        serverAuthCode = parcel.readString()
        email = parcel.readString()
        profileImage = parcel.readString()
        loginToken = parcel.readString()
        id = parcel.readString()
        device = parcel.readParcelable(Device::class.java.classLoader)!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(givenName)
        parcel.writeString(familyName)
        parcel.writeString(displayName)
        parcel.writeString(googleIdToken)
        parcel.writeString(serverAuthCode)
        parcel.writeString(email)
        parcel.writeString(profileImage)
        parcel.writeString(loginToken)
        parcel.writeString(id)
        parcel.writeParcelable(device, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

}

class Device() : Parcelable {

    var token: String? = null
    var type = 0

    constructor(parcel: Parcel) : this() {
        token = parcel.readString()
        type = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(token)
        parcel.writeInt(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Device> {
        override fun createFromParcel(parcel: Parcel): Device {
            return Device(parcel)
        }

        override fun newArray(size: Int): Array<Device?> {
            return arrayOfNulls(size)
        }
    }


}

