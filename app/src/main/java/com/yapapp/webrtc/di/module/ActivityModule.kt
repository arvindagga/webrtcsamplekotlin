package com.yapapp.webrtc.di.module


import com.yapapp.webrtc.view.call.CallActivity
import com.yapapp.webrtc.view.initial.InitialActivity
import com.yapapp.webrtc.view.home.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {


//    @ContributesAndroidInjector(modules = [AndroidSupportInjectionModule::class,InitialFragmentBuilder::class])
//    abstract  fun bindInitialRiderActivity(): InitialRiderActivity

    @get:ContributesAndroidInjector(modules = [FragmentModule::class])
    internal abstract val initialActivity: InitialActivity

    @get:ContributesAndroidInjector(modules = [FragmentModule::class])
    internal abstract val homeActivity: HomeActivity

    @get:ContributesAndroidInjector(modules = [FragmentModule::class])
    internal abstract val callActivity: CallActivity
}