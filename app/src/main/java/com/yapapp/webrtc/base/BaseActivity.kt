package com.yapapp.webrtc.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.yapapp.webrtc.call.rtc.initCapturer
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.layout_calling_view.*
import org.webrtc.RendererCommon
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {


    @Inject
    lateinit var supportFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

    }


    fun showActionBar(bool: Boolean) {
        if (bool)
            supportActionBar?.show()
        else
            supportActionBar?.hide()
    }


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {

        return supportFragmentInjector
    }

    fun setTitle(title: String) {
        // supportActionBar?.title=title
    }

    fun popBackStack() {
        supportFragmentManager.popBackStack()
    }




    fun getCurrentFragment(id: Int): Fragment{

    return    supportFragmentManager.findFragmentById(id)!!

    }



}
