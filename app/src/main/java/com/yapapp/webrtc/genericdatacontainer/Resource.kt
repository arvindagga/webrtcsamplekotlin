package com.example.findnearbyusers.genericdatacontainer

import androidx.annotation.Nullable


// A generic class that contains data and status about loading this data.
class Resource<T> private constructor(
    val status: Status, @param:Nullable @field:Nullable
    val data: T,
    @param:Nullable @field:Nullable val message: String?
) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {

        fun <T> success(data: T): Resource<T> {
            return Resource<T>(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, @Nullable data: T): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(@Nullable data: T): Resource<T> {
            return Resource<T>(Status.LOADING, data, null)
        }
    }
}