package com.yapapp.webrtc.call.rtc

import org.webrtc.SdpObserver
import org.webrtc.SessionDescription


/**
 * for transmitter(initiator)
 *
 * create an offer and set as local description
 *
 */

fun RTCClient.createOffer(){


    peerConnection?.createOffer(object: SdpObserver{
        override fun onSetFailure(p0: String?) {
        }

        override fun onSetSuccess() {


        }

        override fun onCreateSuccess(p0: SessionDescription?) {
            peerConnection?.setLocalDescription(this,p0)
            rtcInterface.onOfferCreated(p0!!)

        }

        override fun onCreateFailure(p0: String?) {
        }
    },defaultConstraints)

}


/**
 * for receiver end
 *
 * set received offer as remote description
 * & generate answer
 */


fun RTCClient.setOffer(sdpType: String, description: String){


    val sdp= SessionDescription(SessionDescription.Type.OFFER, description)

    peerConnection?.setRemoteDescription(object: SdpObserver{
        override fun onSetFailure(p0: String?) {
        }

        override fun onSetSuccess() {

           createAnswer()
        }

        override fun onCreateSuccess(p0: SessionDescription?) {


        }

        override fun onCreateFailure(p0: String?) {
        }
    },sdp)


}



fun RTCClient.createAnswer(){


    peerConnection?.createAnswer(object: SdpObserver{
        override fun onSetFailure(p0: String?) {
        }

        override fun onSetSuccess() {



        }

        override fun onCreateSuccess(p0: SessionDescription?) {
             peerConnection?.setLocalDescription(this,p0)
            rtcInterface.onAnswerCreated(p0!!)

        }

        override fun onCreateFailure(p0: String?) {
        }
    },defaultConstraints)

}



fun RTCClient.setAnswer(sdpType: String, description: String){


    val sdp= SessionDescription(SessionDescription.Type.ANSWER, description)

    peerConnection?.setRemoteDescription(object: SdpObserver{
        override fun onSetFailure(p0: String?) {
        }

        override fun onSetSuccess() {


        }

        override fun onCreateSuccess(p0: SessionDescription?) {


        }

        override fun onCreateFailure(p0: String?) {
        }
    },sdp)


}

