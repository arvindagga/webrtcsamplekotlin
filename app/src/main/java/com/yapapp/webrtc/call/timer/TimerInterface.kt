package com.yapapp.webrtc.call.timer

interface TimerInterface {

    fun onConnectingTimeComplete()
    fun onOutGoingTimeComplete()
    fun onInComingTimeComplete()
}