package com.yapapp.webrtc.view.home.userlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yapapp.webrtc.R
import com.yapapp.webrtc.model.User
import com.yapapp.webrtc.util.loadProfileImageFromURL
import kotlinx.android.synthetic.main.item_users.view.*

class UsersAdapter(var homeFragment: HomeFragment) :
    RecyclerView.Adapter<UsersAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.item_users,
                viewGroup,
                false
            )
        )
    }

    init {
        setHasStableIds(true)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemId(position: Int): Long = position.toLong()


    var data = ArrayList<User>()


    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.bind(data.get(p1), p1)

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: User, position: Int) {
            itemView.apply {

                user.profileImage.let {
                    iv_userimage.loadProfileImageFromURL(
                        context,
                        user.profileImage!!
                    )
                }

                tv_username.text = user.displayName

                if (homeFragment.selectedlist.contains(user.email))
                    itemView.isActivated = true
                else
                    itemView.isActivated = false

            }

            itemView.setOnLongClickListener {
                homeFragment.onItemLongClick(position)
                true
            }

            itemView.setOnClickListener {
                homeFragment.onItemClick(position)
            }


        }
    }

    interface UserListInterface {
        fun onItemClick(position: Int)
        fun onItemLongClick(position: Int)
    }
}