package com.yapapp.webrtc.view.call

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.findnearbyusers.genericdatacontainer.Resource
import com.yapapp.webrtc.call.*
import com.yapapp.webrtc.call.rtc.RTCMediaStreamTrackListener
import com.yapapp.webrtc.model.call.CallModel
import javax.inject.Inject

class CallViewModel @Inject
constructor(val callManager: CallManager, app: Application) : AndroidViewModel(app), CallInterface {


    var callStatusLiveData = MutableLiveData<CallModel>().apply {

        postValue(CallManager.callModel)
    }

    var remoteVideoLiveData = MutableLiveData<Boolean>()
    var remoteAudioLiveData = MutableLiveData<Boolean>()
    var localAudioLiveData = MutableLiveData<Resource<Any?>>()
    var localVideoLiveData = MutableLiveData<Resource<Any?>>()
    var cameraInUseLivedata = MutableLiveData<Resource<Any?>>()

    var viewsSwaped = false
    var enableAudio = true
    var enableVideo = true


    init {

        callManager.callInterface = this
    }

    fun initCall() {
        callManager.startOutgoingCall(
            CallManager.callModel!!.callInitiator,
            CallManager.callModel!!.callReceiver, CallManager.callModel!!.callType
        )
    }


    fun receiveCall() {

        callManager.receiveIncomingCall()

    }


    override fun onCallAccepted() {


        callStatusLiveData.postValue(CallManager.callModel)
//
    }

    override fun onCallEnded() {

        callStatusLiveData.postValue(CallManager.callModel)
    }

    /**
     * accept call
     *
     */

    fun acceptCall() {

        callManager.acceptCall()


    }


    fun endCall() {
        callManager.endCall()
    }

    /**
     * can be call only ongoing call
     */
    fun switchCamera(): MutableLiveData<Resource<Any?>> {
        callManager.switchCamera(object : RTCMediaStreamTrackListener {
            override fun onSuccess(data: Any?) {

                cameraInUseLivedata.postValue(Resource.success(data))
            }

            override fun onError(error: String) {
                cameraInUseLivedata.postValue(Resource.error(error, null))
            }
        })
        return cameraInUseLivedata
    }


    fun setAudioMode() {

        callManager.setAudioMode()
    }


    override fun onRemoteVideoUpdated(isEnabled: Boolean) {
        remoteVideoLiveData.postValue(isEnabled)

    }

    override fun onRemoteAudioUpdated(isEnabled: Boolean) {
        remoteAudioLiveData.postValue(isEnabled)
    }

    fun muteUnmuteAudio(): MutableLiveData<Resource<Any?>> {
        enableAudio = !enableAudio

        callManager.muteUnmuteAudio(enableAudio, object : RTCMediaStreamTrackListener {
            override fun onSuccess(data: Any?) {

                localAudioLiveData.postValue(Resource.success(data))

            }

            override fun onError(error: String) {

                localAudioLiveData.postValue(Resource.error(error, null))

            }
        })

        return localAudioLiveData
    }


    fun enableDisableVideo(): MutableLiveData<Resource<Any?>> {
        enableVideo = !enableVideo

        callManager.enableDisableVideo(enableVideo, object : RTCMediaStreamTrackListener {
            override fun onSuccess(data: Any?) {

                localVideoLiveData.postValue(Resource.success(data))

            }

            override fun onError(error: String) {

                localVideoLiveData.postValue(Resource.error(error, null))

            }
        })

        return localAudioLiveData
    }

}