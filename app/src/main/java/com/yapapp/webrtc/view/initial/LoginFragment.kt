package com.yapapp.webrtc.view.initial

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.rontaxi.retrofit.getErrorMessage
import com.rontaxi.retrofit.tackleError
import com.yapapp.webrtc.R
import com.yapapp.webrtc.WebRTCApp
import com.yapapp.webrtc.base.BaseFragment
import com.yapapp.webrtc.cache.getUser
import com.yapapp.webrtc.model.User
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    lateinit var mGoogleSignInClient: GoogleSignInClient

    val RC_SIGN_IN = 100

    val TAG = "result"

    var fcmToken: String? = null

    @Inject
    lateinit var auth: FirebaseAuth

    @Inject
    lateinit var gso: GoogleSignInOptions

    lateinit var signUpViewModel: SignUpViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun getLayoutRes() = R.layout.activity_login


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // VIEWMODEL CONFIGURATION
        configureViewModel();

        sign_in_button.setOnClickListener {

            signInUsingGoogle()

        }

        getFCMToken()


    }

    private fun configureViewModel() {

        signUpViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(SignUpViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if (getUser(WebRTCApp.context!!) != null) {
            this.findNavController().navigate(R.id.action_loginFragment_to_home)
            activity!!.finish()

        }
        // updateUI(currentUser)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
                // ...
            }
        }
    }

    fun updateUI(user: FirebaseUser?) {
        if (user != null) {

            var data = User()

            data.displayName = user.displayName.toString()
            data.email = user.email.toString()
            data.profileImage = user.photoUrl.toString()
            data.device.token = fcmToken

            // ProfileActivity.kt
            signUpViewModel.signUpUser(data).observe(this, Observer {
                // populateUserProfile(it)

                if (it!!.isSuccessful) {

                    it.body()?.data?.let { it1 -> signUpViewModel.saveUserinCache(it1) }

                    this.findNavController().navigate(R.id.action_loginFragment_to_home)
                } else {
                    tackleError(activity!!, getErrorMessage(it.errorBody()!!))
                }

            })

        }

    }
}
