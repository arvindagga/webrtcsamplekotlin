package com.yapapp.webrtc.call.rtc

import com.yapapp.webrtc.call.rtc.RTCClient.Companion.LOCAL_STREAM_ID
import com.yapapp.webrtc.call.rtc.RTCClient.Companion.LOCAL_TRACK_ID
import org.webrtc.*


fun RTCClient.getVideoCapturer() =
    Camera2Enumerator(context).run {
        deviceNames.find {
            isFrontFacing(it)
        }?.let {
            createCapturer(it, null)
        } ?: throw IllegalStateException()
    }


/**
 * generates local video stream
 */

private fun RTCClient.createLocalVideoTrack(): VideoTrack? {

    val localVideoTrack = peerConnectionFactory.createVideoTrack(LOCAL_TRACK_ID, localVideoSource)

    val localStream = peerConnectionFactory.createLocalMediaStream(LOCAL_STREAM_ID)
    localStream.addTrack(localVideoTrack)
    peerConnection?.addStream(localStream)

    return localVideoTrack

}


/**
 * generates audio stream
 */

private fun RTCClient.createLocalAudioTrack(): AudioTrack? {

    val audioSource = peerConnectionFactory.createAudioSource(audioConstraints)

    val audioTrack = peerConnectionFactory.createAudioTrack("ARDAMSa0", audioSource)
    audioTrack?.setEnabled(false)

    return audioTrack

}


/**
 * this function generates local stream which
 * comprises audio and video stream
 */


fun RTCClient.createLocalStream(isVideoCall: Boolean): MediaStream {

    val localStream = peerConnectionFactory.createLocalMediaStream("ARDAMS")


    if (isVideoCall) {
        localVideoTrack = createLocalVideoTrack()
        localVideoTrack?.setEnabled(true)
    }

    if (localVideoTrack != null)
        localStream.addTrack(localVideoTrack)


    localAudioTrack = createLocalAudioTrack()
    localAudioTrack?.setEnabled(false)

    if (localAudioTrack != null)
        localStream.addTrack(localAudioTrack)




    return localStream
}


fun RTCClient.addStream(isVideoCall: Boolean) {


    localMediaStream = createLocalStream(isVideoCall)

    peerConnection?.addStream(localMediaStream)


    if (isVideoCall) {
        initCapturer(localViewRenderer)
        startCapturing()

        localMediaStream!!.videoTracks[0].addSink(localViewRenderer)
    }

}


fun RTCClient.initCapturer(surfaceViewRenderer: SurfaceViewRenderer) {

    val surfaceTextureHelper =
        SurfaceTextureHelper.create(Thread.currentThread().name, rootEglBase.eglBaseContext)

    videoCapturer.initialize(
        surfaceTextureHelper,
        surfaceViewRenderer.context,
        localVideoSource.capturerObserver
    )


}

fun RTCClient.startCapturing() {

    videoCapturer.startCapture(500, 500, 0)
}

fun RTCClient.switchCamera(listener: RTCMediaStreamTrackListener) {
    videoCapturer.switchCamera(object : CameraVideoCapturer.CameraSwitchHandler {

        override fun onCameraSwitchDone(p0: Boolean) {


            localViewRenderer.setMirror(p0)

            listener.onSuccess(p0)

        }

        override fun onCameraSwitchError(p0: String?) {

            listener.onError(p0!!)

        }
    })
}

fun RTCClient.muteUnmuteAudio(enableAudio: Boolean, listener: RTCMediaStreamTrackListener) {
    var result = localMediaStream!!.audioTracks[0]!!.setEnabled(enableAudio)

    listener.onSuccess(result)
}

fun RTCClient.enableDisableVideo(enableVideo: Boolean, listener: RTCMediaStreamTrackListener) {
    var result = localMediaStream!!.videoTracks[0]!!.setEnabled(enableVideo)

    listener.onSuccess(result)
}