package com.yapapp.webrtc.constants


object SocketConstants{

    const val SOCKET_URL="http://192.168.2.194:3008/"
    const val AUTHENTICATE="authenticate"




    const val MESSAGE_TO_SHOW_KEY="msg_key"

    const val RESPONSE_CODE_KEY="code_key"
    const val RESPONSE_DATA_KEY="response_key"


    // call events

    const val REQUEST_TO_CONNECT="requestToConnect"

    const val RECEIVER_CONNECTED="recieverConnected"

    const val SDP_OFFER="sdpOffer"

    const val SDP_Answer="sdpAnswer"

    const val ICE_CANDIDATES="iceCandidate"

    const val CALL_OPERATIONS="callOperation"

    const val UPDATE_CONTROL="updateControl"




}

