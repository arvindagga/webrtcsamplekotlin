package com.yapapp.webrtc.model.home

import com.rontaxi.retrofit.ApiService
import javax.inject.Inject

class HomeRepo @Inject
constructor(val apiService: ApiService) {
    suspend fun getUsers() = apiService.getUsers()
}