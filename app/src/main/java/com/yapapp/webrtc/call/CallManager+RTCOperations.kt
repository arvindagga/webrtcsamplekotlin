package com.yapapp.webrtc.call

import com.yapapp.webrtc.call.rtc.RTCMediaStreamTrackListener
import com.yapapp.webrtc.call.rtc.switchCamera
import org.webrtc.SurfaceViewRenderer

fun CallManager.switchCamera(listener: RTCMediaStreamTrackListener) {

    rtcClient.switchCamera(listener)
}


fun CallManager.addSurfaceViewRenderer(
    localView: SurfaceViewRenderer? = null,
    remoteView: SurfaceViewRenderer? = null
) {
    rtcClient.addSurfaceViewRenderer(localView, remoteView)
}

