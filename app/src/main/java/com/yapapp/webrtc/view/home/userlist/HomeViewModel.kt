package com.yapapp.webrtc.view.home.userlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.yapapp.webrtc.model.home.HomeRepo
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import com.yapapp.webrtc.sockets.SocketManager

class HomeViewModel @Inject
constructor(private val homeRepo: HomeRepo, val socketManager: SocketManager) : ViewModel() {


    init {
        socketManager.connect()
    }

    var userSelection:MutableLiveData<List<String>> = MutableLiveData()

    var users = liveData(Dispatchers.IO) {
        val response = homeRepo.getUsers()
        emit(response)
    }







}
