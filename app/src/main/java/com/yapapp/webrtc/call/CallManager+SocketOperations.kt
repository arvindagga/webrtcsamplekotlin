package com.yapapp.webrtc.call

import com.yapapp.webrtc.call.audio.AudioManager
import com.yapapp.webrtc.model.User


fun CallManager.startOutgoingCall(fromUser: User, toUser: User, callType: CallManager.CallType) {



    startConnectingTimer()

    playAudio(AudioManager.CALL_AUDIO_TYPE.CONNECTING)

    callSocketManager.createRequestToConnect(fromUser, toUser, callType.value)

    if (CallManager.callModel!!.callType == CallManager.CallType.VIDEO) {
        rtcClient.createPeerConnection(true)

    } else {
        rtcClient.createPeerConnection(false)

    }

}


fun CallManager.receiveIncomingCall() {

    startInComingTimer()

    callSocketManager.receiverConnectedToSocket(
        CallManager.callModel!!.callInitiator.id!!,
        CallManager.callModel!!.callReceiver.id!!,
        true
    )
    if (CallManager.callModel!!.callType == CallManager.CallType.VIDEO) {
        rtcClient.createPeerConnection(true)

    } else {
        rtcClient.createPeerConnection(false)

    }
}


fun CallManager.sendBusyMessage(currentUser: User, callInitiator: User) {
    callSocketManager.receiverConnectedToSocket(currentUser.id!!, callInitiator.id!!, false)
}


fun CallManager.endCall() {

    cancelAllTimer()
    stopAudio()

    callSocketManager.sendCallOperations(
        CallManager.callModel!!.callInitiator.id!!,
        CallManager.callModel!!.callReceiver.id!!,
        CallManager.CallOperations.END_CALL.value
    )
    clearCallData()

}


fun CallManager.acceptCall() {

    cancelAllTimer()
    stopAudio()
    callSocketManager.sendCallOperations(
        CallManager.callModel!!.callInitiator.id!!,
        CallManager.callModel!!.callReceiver.id!!,
        CallManager.CallOperations.ACCEPT.value
    )
}













