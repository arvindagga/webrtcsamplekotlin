package com.yapapp.webrtc.view.call

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.myapplication.di.viewmodelprovider.ViewModelFactory
import com.yapapp.webrtc.R
import com.yapapp.webrtc.base.BaseFragment
import com.yapapp.webrtc.call.CallManager
import com.yapapp.webrtc.util.AllPurposeTouchListener
import kotlinx.android.synthetic.main.layout_calling_view.*
import kotlinx.android.synthetic.main.ringing_call.*
import javax.inject.Inject

class CallFragment : BaseFragment() {

    lateinit var manswerCallSwipeListener: AllPurposeTouchListener

    override fun getLayoutRes() = R.layout.fragment_call

    var callType = 0


    lateinit var callViewModel: CallViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configureViewModel()

        if (CallManager.callModel?.callState == CallManager.CallState.OUT_GOING_VIDEO
            || CallManager.callModel?.callState == CallManager.CallState.OUT_GOING_AUDIO
        ) {

            answer_btn.hide()
            group_incoming_call.visibility = View.INVISIBLE

            reject_btn.setOnClickListener {


                activity!!.finish()
                callViewModel.endCall()

            }
            callViewModel.initCall()

        } else if (CallManager.callModel?.callState == CallManager.CallState.IN_COMING_VIDEO
            || CallManager.callModel?.callState == CallManager.CallState.IN_COMING_AUDIO
        ) {
            reject_btn.hide()

            addAnswerCallSwipeListner()

            val bounceAnimation =
                AnimationUtils.loadAnimation(context!!, R.anim.bounce_animation)

            answer_btn.startAnimation(bounceAnimation)

            callViewModel.receiveCall()

        }

        /**
         * if this is video call only then add surfaceviewrenderers to the view
         */
        if (CallManager.callModel?.callType == CallManager.CallType.VIDEO)
            startRendering()
        else {
            // audio call
            text_status.text = getString(R.string.audio_calling)
        }

    }


    /**
     * Answers incoming call and changes the ui accordingly
     */
    fun activateCall() {

        //unmute  audio
        muteUnmuteAudio()

        large_view.removeAllViews()
        this.findNavController().navigate(R.id.action_callFragment_to_ongoingCallFragment)

    }


    private fun configureViewModel() {

        callViewModel =
            ViewModelProviders.of(activity!!, viewModelFactory).get(CallViewModel::class.java)
    }


    private fun startRendering() {
        large_view.addView((activity as CallActivity).localSurfaceRendrer)
        //remote_view.addView((activity as CallActivity).remoteSurfaceRendrer)
    }


}
