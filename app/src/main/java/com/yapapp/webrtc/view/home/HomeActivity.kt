package com.yapapp.webrtc.view.home

import android.os.Bundle
import com.yapapp.webrtc.R
import com.yapapp.webrtc.base.BaseActivity

class HomeActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

    }

}
