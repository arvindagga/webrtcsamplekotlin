package com.yapapp.webrtc.view.call

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.di.viewmodelprovider.ViewModelFactory
import com.yapapp.webrtc.R
import com.yapapp.webrtc.base.BaseFragment
import com.yapapp.webrtc.call.CallManager
import com.yapapp.webrtc.util.OnSwipeTouchListener
import kotlinx.android.synthetic.main.layout_calling_view.*
import kotlinx.android.synthetic.main.on_going_video_call.*
import javax.inject.Inject

class OngoingCallFragment : BaseFragment() {

    lateinit var callViewModel: CallViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun getLayoutRes() = R.layout.fragment_ongoing_call

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configureViewModel()

        callViewModel.setAudioMode()


        /**
         * if this is video call only then add surfaceviewrenderers to the view
         */
        if (CallManager.callModel?.callType == CallManager.CallType.VIDEO)
            startRendering() else
            group_audio_views.visibility = View.VISIBLE

        callViewModel.viewsSwaped = false


        small_view.setOnTouchListener(object : OnSwipeTouchListener(context!!) {

            override fun onClick() {
                super.onClick()
                swapViews()
            }

            override fun onMove(e2: MotionEvent?) {
                super.onMove(e2)
                if (e2 != null) {
                    small_view.y = e2.rawY - small_view.height / 2
                    small_view.x = e2.rawX - small_view.width / 2
                }

            }

        })

        reject_btn.setOnClickListener {


            activity!!.finish()
            callViewModel.endCall()

        }

//        ongoing_call_layout.setOnClickListener {
//
//            if (group_call_controls.visibility == View.VISIBLE) {
//                group_call_controls.visibility = View.GONE
//            } else {
//                group_call_controls.visibility = View.VISIBLE
//            }
//
//        }

        button_mute.setOnClickListener {
            muteUnmuteAudio()
        }

        button_switchCamera.setOnClickListener {

            switchCamera()
        }

        iv_enable_disable_video.setOnClickListener {

            enableDisableVideo()

        }

        callViewModel.remoteVideoLiveData.observe(this, Observer {

            if (!it)
                tv_remote_video_status.visibility = View.VISIBLE
            else
                tv_remote_video_status.visibility = View.GONE

        })

        callViewModel.remoteAudioLiveData.observe(this, Observer {


            if (!it)
                tv_remote_audio_status.visibility = View.VISIBLE
            else
                tv_remote_audio_status.visibility = View.GONE

        })

    }

    private fun configureViewModel() {

        callViewModel =
            ViewModelProviders.of(activity!!, viewModelFactory).get(CallViewModel::class.java)
    }

    private fun startRendering() {

        small_view.visibility = View.VISIBLE

        small_view.addView((activity as CallActivity).localSurfaceRendrer)
        large_view.addView((activity as CallActivity).remoteSurfaceRendrer)

    }

}
