package com.yapapp.webrtc.view.home.userlist

import android.Manifest
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.rontaxi.retrofit.getErrorMessage
import com.rontaxi.retrofit.tackleError
import com.rontaxi.util.InternetCheck
import com.yapapp.webrtc.R
import com.yapapp.webrtc.base.BaseFragment
import com.yapapp.webrtc.cache.getUser
import com.yapapp.webrtc.model.User
import com.yapapp.webrtc.util.PermissionUtils
import com.yapapp.webrtc.view.call.CallActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_no_data.*
import javax.inject.Inject

class HomeFragment : BaseFragment(),
    UsersAdapter.UserListInterface {
    override fun onItemClick(position: Int) {

        var list: ArrayList<String> =
            homeViewModel.userSelection.value as ArrayList<String>


        if (list.contains(usersAdapter.data.get(position).email)) {
            var index = list.indexOf(usersAdapter.data.get(position).email)
            list.removeAt(index)
            homeViewModel.userSelection.postValue(list)
            usersAdapter.notifyItemChanged(position)

        }

    }

    override fun onItemLongClick(position: Int) {
        var list: ArrayList<String> =
            homeViewModel.userSelection.value as ArrayList<String>

        if (list.size >= 1) {
            Toasty.error(
                activity!!,
                getString(R.string.user_limit_exceed),
                Toast.LENGTH_SHORT,
                true
            )
                .show()
            return
        }
        if (!list.contains(usersAdapter.data.get(position).email)) {
            list.add(usersAdapter.data.get(position).email!!)
            homeViewModel.userSelection.postValue(list)
            usersAdapter.notifyItemChanged(position)

        }
    }

    @Inject
    lateinit var auth: FirebaseAuth

    @Inject
    lateinit var gso: GoogleSignInOptions

    var selectedlist = java.util.ArrayList<String>()

    lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun getLayoutRes() = R.layout.fragment_home

    lateinit var homeViewModel: HomeViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var usersAdapter: UsersAdapter
    var userList = ArrayList<User>()

    val PERMISSION_REQUEST_CODE = 101


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // VIEWMODEL CONFIGURATION
        configureViewModel();

        btnLogout.setOnClickListener {

            logOut()
        }

        addObservers()

        setAdapter()

        imgBtnClose.setOnClickListener {

            clearAllSelections()
        }

        ibAudioCall.setOnClickListener {

            if (!InternetCheck.isConnectedToInternet(context!!))
                return@setOnClickListener

            if (homeViewModel.userSelection.value!!.isNotEmpty()) {


                val user = userList.singleOrNull {

                    it.email!!.equals(homeViewModel.userSelection.value!!.get(0))
                }

                user?.let {
                    CallActivity.initAudioCall(context!!, getUser(context!!)!!, it)
                }

            }
        }


        ibVideoCall.setOnClickListener {

            if (!InternetCheck.isConnectedToInternet(context!!))
                return@setOnClickListener

            if (PermissionUtils.hasPermission(
                    activity!!,
                    Manifest.permission.CAMERA
                ) && PermissionUtils.hasPermission(activity!!, Manifest.permission.RECORD_AUDIO)
                && PermissionUtils.hasPermission(
                    activity!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                if (homeViewModel.userSelection.value!!.isNotEmpty()) {


                    val user = userList.singleOrNull {

                        it.email!!.equals(homeViewModel.userSelection.value!!.get(0))
                    }

                    user?.let {
                        CallActivity.initVideoCall(context!!, getUser(context!!)!!, it)
                    }

                }
            } else {
                PermissionUtils.requestPermissions(
                    activity!!,
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    PERMISSION_REQUEST_CODE
                )
            }


        }


        // this.findNavController().navigate(R.id.action_homeFragment_to_callActivity)

    }

    private fun setAdapter() {
        rv_users.layoutManager = LinearLayoutManager(activity)
        usersAdapter = UsersAdapter(this)
        rv_users.adapter = usersAdapter
        usersAdapter.data = userList

    }


    private fun addObservers() {

        if (!InternetCheck.isConnectedToInternet(context!!))
            return


        homeViewModel.users.observe(this, Observer {

            if (it.isSuccessful) {

                it.body()?.data.let {

                    if (it?.size!! > 0) {

                        observeUserSelection()

                        noDataLayout.visibility = View.GONE
                        rv_users.visibility = View.VISIBLE
                        userList.clear()
                        userList.addAll(it.filter {

                            !it.id.equals(getUser(context!!)!!.id)
                        })

                        usersAdapter.data = userList

                        usersAdapter.notifyDataSetChanged()
                    } else {
                        tvNoData.text = getString(R.string.no_booking_data)
                        noDataLayout.visibility = View.VISIBLE
                        rv_users.visibility = View.GONE
                    }

                }

            } else {
                tackleError(activity!!, getErrorMessage(it.errorBody()!!))
            }

        })
    }

    private fun configureViewModel() {

        homeViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)

        homeViewModel.userSelection.value = selectedlist
    }

    override fun onResume() {
        super.onResume()
        clearAllSelections()
    }
}
