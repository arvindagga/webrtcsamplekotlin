package com.yapapp.webrtc.call.rtc

interface RTCMediaStreamTrackListener {

    fun onSuccess(data: Any? = null)

    fun onError(error: String)

}