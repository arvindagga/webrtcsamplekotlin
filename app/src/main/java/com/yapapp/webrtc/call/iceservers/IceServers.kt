package com.yapapp.webrtc.call.iceservers

import org.json.JSONObject
import org.webrtc.PeerConnection

private val iceServersList="{\n" +
        "\t\"STUN\": [{\n" +
        "\t\t\"url\": \"stun:stun01.sipphone.com\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.ekiga.net\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.fwdnet.net\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.ideasip.com\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.iptel.org\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.rixtelecom.se\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.schlund.de\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.l.google.com:19302\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun1.l.google.com:19302\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun2.l.google.com:19302\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun3.l.google.com:19302\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun4.l.google.com:19302\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stunserver.org\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.softjoys.com\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.voiparound.com\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.voipbuster.com\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.voipstunt.com\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.voxgratia.org\"\n" +
        "\t}, {\n" +
        "\t\t\"url\": \"stun:stun.xten.com\"\n" +
        "\t}],\n" +
        "\t\"TURN\": [{\n" +
        "\t\t\t\t\"url\": \"turn:52.40.28.77:3478?transport=udp\",\n" +
        "\t\t\t\t\"credential\": \"turn_2013_pass\",\n" +
        "\t\t\t\t\"username\": \"turn_2013_user\"\n" +
        "\t\t\t}, {\n" +
        "\t\t\t\t\"url\": \"turn:numb.viagenie.ca\",\n" +
        "\t\t\t\t\"credential\": \"muazkh\",\n" +
        "\t\t\t\t\"username\": \"webrtc@live.com\"\n" +
        "\t\t\t}, {\n" +
        "\t\t\t\t\"url\": \"turn:192.158.29.39:3478?transport=udp\",\n" +
        "\t\t\t\t\"credential\": \"JZEOEt2V3Qb0y27GRntt2u2PAYA=\",\n" +
        "\t\t\t\t\"username\": \"28224511:1379330808\"\n" +
        "\t\t\t}, {\n" +
        "\t\t\t\t\"url\": \"turn:192.158.29.39:3478?transport=tcp\",\n" +
        "\t\t\t\t\"credential\": \"JZEOEt2V3Qb0y27GRntt2u2PAYA=\",\n" +
        "\t\t\t\t\"username\": \"28224511:1379330808”}\n" +
        "\t\t\t  ,\n" +
        "\t\t\t\t{\"url \": \"turn: turn.anyfirewall.com:443?transport=tcp\",\n" +
        "\t\t\t     \"credential\": \"webrtc\"\n" +
        "\t\t\t  , \"username\": \"webrtc”\n" +
        "\t\t\t\t},\n" +
        "\t\t\t\t{\n" +
        "\t\t\t\t\t\"url\": \"turn:turn.bistri.com:80\",\n" +
        "\t\t\t\t\t\"credential\": \"homeo\",\n" +
        "\t\t\t\t\t\"username\": \"homeo”}]}";



  fun getIceServersList() : List<PeerConnection.IceServer>{






     var serversList=ArrayList<PeerConnection.IceServer>()

     val jsonObject=JSONObject(str)

     val stunServers=jsonObject.getJSONArray("STUN")

    // val turnServers=jsonObject.getJSONArray("TURN")


     for(i in 0 until (stunServers.length()-1)){

         val jsonObject=stunServers[i] as JSONObject

         var iceServer=PeerConnection.IceServer
             .builder(jsonObject.getString("url"))
             .createIceServer()

         serversList.add(iceServer)
     }


    /* for(i in 0 until (turnServers.length()-1)){

         val jsonObject=turnServers[i] as JSONObject

         var iceServer=PeerConnection.IceServer
             .builder(jsonObject.getString("url"))
             .setUsername(jsonObject.getString("credential"))
             .setPassword(jsonObject.getString("username"))
             .createIceServer()

         serversList.add(iceServer)
     }*/




    return serversList

 }




fun getICEServer(url: String, userName: String, password: String): PeerConnection.IceServer{
    var iceServer=PeerConnection.IceServer
        .builder(url)
        .setUsername(userName)
        .setPassword(password)
        .createIceServer()

    return iceServer
}

fun getICEServer(url: String): PeerConnection.IceServer{
    var iceServer=PeerConnection.IceServer
        .builder(url)
        .createIceServer()
    return iceServer
}




val str="{\"STUN\":[{\"url\":\"stun:stun01.sipphone.com\"},{\"url\":\"stun:stun.ekiga.net\"},{\"url\":\"stun:stun.fwdnet.net\"},{\"url\":\"stun:stun.ideasip.com\"},{\"url\":\"stun:stun.iptel.org\"},{\"url\":\"stun:stun.rixtelecom.se\"},{\"url\":\"stun:stun.schlund.de\"},{\"url\":\"stun:stun.l.google.com:19302\"},{\"url\":\"stun:stun1.l.google.com:19302\"},{\"url\":\"stun:stun2.l.google.com:19302\"},{\"url\":\"stun:stun3.l.google.com:19302\"},{\"url\":\"stun:stun4.l.google.com:19302\"},{\"url\":\"stun:stunserver.org\"},{\"url\":\"stun:stun.softjoys.com\"},{\"url\":\"stun:stun.voiparound.com\"},{\"url\":\"stun:stun.voipbuster.com\"},{\"url\":\"stun:stun.voipstunt.com\"},{\"url\":\"stun:stun.voxgratia.org\"},{\"url\":\"stun:stun.xten.com\"}]}"



