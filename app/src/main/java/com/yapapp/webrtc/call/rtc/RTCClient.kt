package com.yapapp.webrtc.call.rtc

import android.content.Context
import android.util.Log
import com.yapapp.webrtc.WebRTCApp
import com.yapapp.webrtc.call.iceservers.getIceServersList
import org.webrtc.*

class RTCClient(val context: Context, val rootEglBase: EglBase) : PeerConnection.Observer {


    val TAG = "RTC_MANAGER"

    lateinit var rtcInterface: RTCInterface

    lateinit var localViewRenderer: SurfaceViewRenderer

    lateinit var remoteViewRenderer: SurfaceViewRenderer


    private val AUDIO_CODEC_PARAM_BITRATE = "maxaveragebitrate"
    private val AUDIO_ECHO_CANCELLATION_CONSTRAINT = "googEchoCancellation"
    private val AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT = "googAutoGainControl"
    private val AUDIO_HIGH_PASS_FILTER_CONSTRAINT = "googHighpassFilter"
    private val AUDIO_NOISE_SUPPRESSION_CONSTRAINT = "googNoiseSuppression"
    private val AUDIO_LEVEL_CONTROL_CONSTRAINT = "levelControl"


    val defaultConstraints: MediaConstraints by lazy {
        MediaConstraints()
    }.apply {
        this.value.mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
        this.value.mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"))
    }

    companion object {
        const val LOCAL_TRACK_ID = "local_track"
        const val LOCAL_STREAM_ID = "local_track"
    }

    val videoCapturer: CameraVideoCapturer by lazy { getVideoCapturer() }
    val localVideoSource by lazy { peerConnectionFactory.createVideoSource(false) }
    val peerConnectionFactory by lazy { buildPeerConnectionFactory() }


    val audioConstraints: MediaConstraints by lazy {

        MediaConstraints()
    }.apply {

        this.value.mandatory.add(
            MediaConstraints.KeyValuePair(
                AUDIO_ECHO_CANCELLATION_CONSTRAINT,
                "true"
            )
        )
        this.value.mandatory.add(
            MediaConstraints.KeyValuePair(
                AUDIO_AUTO_GAIN_CONTROL_CONSTRAINT,
                "true"
            )
        )
        this.value.mandatory.add(
            MediaConstraints.KeyValuePair(
                AUDIO_HIGH_PASS_FILTER_CONSTRAINT,
                "false"
            )
        )
        this.value.mandatory.add(
            MediaConstraints.KeyValuePair(
                AUDIO_NOISE_SUPPRESSION_CONSTRAINT,
                "true"
            )
        )
    }


    private fun buildPeerConnectionFactory(): PeerConnectionFactory {
        return PeerConnectionFactory
            .builder()
            .setVideoDecoderFactory(DefaultVideoDecoderFactory(rootEglBase.eglBaseContext))
            .setVideoEncoderFactory(
                DefaultVideoEncoderFactory(
                    rootEglBase.eglBaseContext,
                    true,
                    true
                )
            )
            .setOptions(PeerConnectionFactory.Options().apply {
                disableEncryption = true
                disableNetworkMonitor = true
            })
            .createPeerConnectionFactory()
    }


    var localVideoTrack: VideoTrack? = null

    var localAudioTrack: AudioTrack? = null


    // var peerConnectionFactory: PeerConnectionFactory

    var peerConnection: PeerConnection? = null

    var localMediaStream: MediaStream? = null


    init {

//        PeerConnectionFactory.initialize(
//            PeerConnectionFactory.InitializationOptions.builder(WebRTCApp.context)
//                .setEnableVideoHwAcceleration(true).createInitializationOptions()
//        )
        val options = PeerConnectionFactory.InitializationOptions.builder(WebRTCApp.context)
            .setEnableInternalTracer(true)
            .setFieldTrials("WebRTC-H264HighProfile/Enabled/")
            .createInitializationOptions()
        PeerConnectionFactory.initialize(options)


//        peerConnectionFactory = PeerConnectionFactory
//            .builder()
//            .createPeerConnectionFactory()
    }


    fun createPeerConnection(isVideoCall: Boolean) {
        peerConnection = peerConnectionFactory.createPeerConnection(
            getRTCConfiguration(),
            this
        )

        addStream(isVideoCall)
    }


    private fun getRTCConfiguration(): PeerConnection.RTCConfiguration {

        val rtcConfig = PeerConnection.RTCConfiguration(getIceServersList())
        rtcConfig.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED
        rtcConfig.bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE
        rtcConfig.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE
        rtcConfig.continualGatheringPolicy =
            PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY
        rtcConfig.keyType = PeerConnection.KeyType.ECDSA

        return rtcConfig
    }


    // callbacks from rtc
    override fun onIceCandidate(p0: IceCandidate?) {

        rtcInterface.onICECandidateGenerated(p0!!.sdpMid, p0.sdpMLineIndex, p0.sdp, p0.serverUrl)

        Log.e(TAG, "ICE_CANDIDATE: url" + p0!!.serverUrl + "\nMid" + p0!!.sdpMid)

    }

    override fun onDataChannel(p0: DataChannel?) {
    }

    override fun onIceConnectionReceivingChange(p0: Boolean) {
    }

    override fun onIceConnectionChange(p0: PeerConnection.IceConnectionState?) {
        Log.e(TAG, "ICE_CONNECTION: " + p0.toString())


        when (p0) {

            PeerConnection.IceConnectionState.CONNECTED -> {


            }

        }

    }

    override fun onIceGatheringChange(p0: PeerConnection.IceGatheringState?) {

        Log.e(TAG, "ICE_CANDIDATES: " + p0.toString())
    }

    override fun onAddStream(p0: MediaStream?) {

        if (p0!!.videoTracks.size > 0) {
            p0.videoTracks[0].addSink(remoteViewRenderer)
        }
        Log.e(TAG, "MediaStream: url" + p0!!.id)
    }

    override fun onSignalingChange(p0: PeerConnection.SignalingState?) {


        Log.e(TAG, "SIGNALLING STATE: " + p0.toString())
    }

    override fun onIceCandidatesRemoved(p0: Array<out IceCandidate>?) {

    }

    override fun onRemoveStream(p0: MediaStream?) {
    }

    override fun onRenegotiationNeeded() {
    }

    override fun onAddTrack(p0: RtpReceiver?, p1: Array<out MediaStream>?) {
        Log.e(TAG, "AddTrack: url" + p0!!.id())

    }


    fun resetRTC() {

//        peerConnection?.dispose()
        videoCapturer?.stopCapture()

        videoCapturer?.dispose()

//        mediaStream?.dispose()

        peerConnection?.close()

        peerConnection = null

    }

    fun addSurfaceViewRenderer(
        localView: SurfaceViewRenderer?,
        remoteView: SurfaceViewRenderer?
    ) {

        if (localView != null) {
            localView.setMirror(true)
            localView.setEnableHardwareScaler(true)
            localView.init(rootEglBase.eglBaseContext, null)
            localView.setZOrderMediaOverlay(true)
            localView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)


            localViewRenderer = localView

        }


        if (remoteView != null) {
            remoteView.setMirror(true)
            remoteView.setEnableHardwareScaler(true)
            remoteView.init(rootEglBase.eglBaseContext, null)
            remoteView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)

            remoteViewRenderer = remoteView
        }
    }


}