package com.yapapp.webrtc.base

import android.content.Context
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment: Fragment() {
    var baseView: View?=null
    var isLoaded=false

    @LayoutRes
    protected abstract fun getLayoutRes(): Int



    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
        isLoaded=true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(baseView==null)
            baseView=inflater.inflate(getLayoutRes(),container,false)
        return baseView
    }

    fun setTitle(title: String){
        (activity as BaseActivity).setTitle(title)
    }

    fun popBackStack(){
        (activity as BaseActivity).popBackStack()
    }
}