package com.yapapp.webrtc.view.call

import androidx.lifecycle.Observer
import com.example.findnearbyusers.genericdatacontainer.Resource
import kotlinx.android.synthetic.main.layout_calling_view.*
import kotlinx.android.synthetic.main.on_going_call.button_mute
import kotlinx.android.synthetic.main.on_going_video_call.*


/**
 *
 * This function is used to mute/unmute Audio based on the above param value
 */
fun OngoingCallFragment.muteUnmuteAudio() {
    callViewModel.muteUnmuteAudio().observe(this, Observer {

        if (it.status == Resource.Status.SUCCESS) {
            /**
             * @param it.data  -  true // Successful
             * @param it.data  -  false // Unsuccessful
             */


            button_mute.isActivated = !callViewModel.enableAudio

        }

    })

}


/**
 *
 * enable/disable video
 */
fun OngoingCallFragment.enableDisableVideo() {
    callViewModel.enableDisableVideo().observe(this, Observer {

        if (it.status == Resource.Status.SUCCESS) {
            /**
             * @param it.data  -  true // Successful
             * @param it.data  -  false // Unsuccessful
             */


            iv_enable_disable_video.isActivated = !callViewModel.enableVideo

        }

    })

}


/**
 * Function to switch between front and real camera
 */
fun OngoingCallFragment.switchCamera() {

    callViewModel.switchCamera().observe(this, Observer {


        if (it.status == Resource.Status.SUCCESS) {
            /**
             * @param it.data  -  true // Front Camera
             * @param it.data  -  false // Back Camera
             */

        }

    })
}

fun OngoingCallFragment.swapViews() {
    callViewModel.viewsSwaped = !callViewModel.viewsSwaped

    small_view.removeAllViews()
    large_view.removeAllViews()
    if (callViewModel.viewsSwaped) {

        small_view.addView((activity as CallActivity).remoteSurfaceRendrer)
        (activity as CallActivity).remoteSurfaceRendrer.setZOrderMediaOverlay(true)
        (activity as CallActivity).localSurfaceRendrer.setZOrderMediaOverlay(false)
        large_view.addView((activity as CallActivity).localSurfaceRendrer)
    } else {
        large_view.addView((activity as CallActivity).remoteSurfaceRendrer)
        small_view.addView((activity as CallActivity).localSurfaceRendrer)
        (activity as CallActivity).localSurfaceRendrer.setZOrderMediaOverlay(true)
        (activity as CallActivity).remoteSurfaceRendrer.setZOrderMediaOverlay(false)

    }
}