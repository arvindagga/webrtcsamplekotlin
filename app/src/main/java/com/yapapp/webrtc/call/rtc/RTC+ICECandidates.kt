package com.yapapp.webrtc.call.rtc

import org.webrtc.IceCandidate

fun RTCClient.processICECandidates(sdpMID: String, sdpMLineIndex: Int, sdp: String, serverUrl: String){

    val iceCandidate=IceCandidate(sdpMID,sdpMLineIndex, sdp)
    peerConnection?.addIceCandidate(iceCandidate)
}