package com.yapapp.webrtc.call.rtc

import org.webrtc.SessionDescription

interface RTCInterface {


    fun onOfferCreated(sdp: SessionDescription)
    fun onAnswerCreated(sdp: SessionDescription)
    fun onICECandidateGenerated(sdpMid: String,sdpMLineIndex: Int,sdp: String,serverUrl: String)

}