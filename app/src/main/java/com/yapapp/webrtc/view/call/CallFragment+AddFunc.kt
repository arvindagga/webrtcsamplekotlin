package com.yapapp.webrtc.view.call

import androidx.lifecycle.Observer
import com.example.findnearbyusers.genericdatacontainer.Resource
import com.yapapp.webrtc.util.AllPurposeTouchListener
import kotlinx.android.synthetic.main.ringing_call.*


/**
 * @param muteAudio
 *
 * This function is used to mute/unmute Audio based on the above param value
 */
fun CallFragment.muteUnmuteAudio() {

    callViewModel.enableAudio = false

    callViewModel.muteUnmuteAudio().observe(this, Observer {

        if (it.status == Resource.Status.SUCCESS) {
            /**
             * @param it.data  -  true // Successful
             * @param it.data  -  false // Unsuccessful
             */

        }

    })

}

fun CallFragment.addAnswerCallSwipeListner() {

    manswerCallSwipeListener = object : AllPurposeTouchListener(context!!) {

        override fun onSwipeRight() {
            //activateCall()
        }

        override fun onSwipeLeft() {
            // activateCall()
        }

        override fun onSwipeTop() {

            callViewModel.acceptCall()
            activateCall()
        }

        override fun onSwipeBottom() {

            callViewModel.endCall()

            /**
             * Reject Incomg Call
             */
            activity!!.finish()

        }

    }
    answer_btn.setOnTouchListener(manswerCallSwipeListener)
}

