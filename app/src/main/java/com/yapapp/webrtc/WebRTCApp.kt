package com.yapapp.webrtc

import android.app.Activity
import android.app.Application
import android.content.Context
import com.example.myapplication.di.component.DaggerAppComponent
//import com.example.myapplication.di.component.DaggerAppComponent
import com.google.firebase.FirebaseApp
import com.yapapp.webrtc.call.CallManager
import com.yapapp.webrtc.call.callsockets.CallSocketEventsManager
import com.yapapp.webrtc.sockets.SocketManager
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class WebRTCApp : Application(), HasActivityInjector {



    @Inject
    lateinit var callSocketManager: CallSocketEventsManager

    @Inject
    lateinit var callManager: CallManager



    companion object {
        var context: Context? = null
    }




    override fun onCreate() {
        super.onCreate()

        context = this

        FirebaseApp.initializeApp(this);

        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)


        callSocketManager.startListening()

    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return dispatchingAndroidInjector
    }
}

