package com.yapapp.webrtc.call.audio

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri

class AudioManager(val context: Context, val audioManager: AudioManager) {


    var audioModeBeforeCall: Int? = null


    var mediaPlayer: MediaPlayer? = null
    val audioMap = HashMap<Int, String>().apply {

        this.put(
            CALL_AUDIO_TYPE.CONNECTING.value,
            "android.resource://" + context.packageName + "/raw/connecting"
        )
        this.put(
            CALL_AUDIO_TYPE.OUT_GOING.value,
            "android.resource://" + context.packageName + "/raw/ring"
        )
        this.put(
            CALL_AUDIO_TYPE.IN_COMING.value,
            "android.resource://" + context.packageName + "/raw/ring"
        )
    }


    fun setAudio(audioType: CALL_AUDIO_TYPE) {


        if (audioModeBeforeCall == null)
            audioModeBeforeCall = audioManager.mode


        reset()

        if (mediaPlayer == null)
            setMediaPlayer()



        audioManager.mode = AudioManager.RINGER_MODE_NORMAL

        prepareAndPlay(audioType, true)


    }


    val audioAttributes: AudioAttributes by lazy {

        AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
            .build()

    }


    private fun prepareAndPlay(audioType: CALL_AUDIO_TYPE, loop: Boolean) {


        mediaPlayer?.setAudioAttributes(audioAttributes)
        mediaPlayer?.setDataSource(context, Uri.parse(audioMap.get(audioType.value)))
        mediaPlayer?.prepare()
        mediaPlayer?.isLooping = true
        mediaPlayer?.start()

    }


    private fun setMediaPlayer() {

        mediaPlayer = MediaPlayer()

        /*mediaPlayer!!.setOnCompletionListener {

            onAudioComplete()
        }*/


    }

    fun reset() {

        mediaPlayer?.stop()
        mediaPlayer?.release()

        mediaPlayer = null
    }


    fun resetMode() {

        audioModeBeforeCall?.let {
            audioManager.mode == it
            audioModeBeforeCall = null

        }
    }


    /**
     * this function is used in communication mode
     * to use in ear pieces
     */

    fun switchToEarPieces() {

        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION)
        audioManager.isSpeakerphoneOn = false
    }


    /**
     * this function is used in communication mode
     * to use in speaker
     */
    fun switchToSpeaker() {

        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION)
        audioManager.isSpeakerphoneOn = true
    }


    /**
     * type of AUDIO Tones
     *
     */
    enum class CALL_AUDIO_TYPE(val value: Int) {
        CONNECTING(1),
        OUT_GOING(2),
        IN_COMING(3),
        ON_GOING(4),
        HOLD(5)
    }


}