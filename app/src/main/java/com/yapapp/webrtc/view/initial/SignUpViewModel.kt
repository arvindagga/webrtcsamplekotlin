package com.yapapp.webrtc.view.initial

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.yapapp.webrtc.WebRTCApp
import com.yapapp.webrtc.cache.saveToken
import com.yapapp.webrtc.cache.saveUser
import com.yapapp.webrtc.model.signup.LoginResponseModel
import com.yapapp.webrtc.model.signup.SignUpRepo
import com.yapapp.webrtc.model.User
import kotlinx.coroutines.Dispatchers
import retrofit2.Response
import javax.inject.Inject

class SignUpViewModel @Inject
constructor(private val sigUpRepo: SignUpRepo) : ViewModel() {


    fun signUpUser(data: User): LiveData<Response<LoginResponseModel>> {

        return liveData(Dispatchers.IO) {
            val response = sigUpRepo.signUpUser(data)

            emit(response)
        }
    }


    fun saveUserinCache(user: User) {

        saveUser(WebRTCApp.context!!, user)
        user.loginToken?.let { saveToken(WebRTCApp.context!!, it) }
    }


}