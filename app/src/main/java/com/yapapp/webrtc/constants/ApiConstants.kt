package com.dizzipay.railsbank.data.remote

object ApiConstants {

    const val BASE_URL = "http://192.168.2.194:3008/"



    //new base url
    // const val BASE_URL ="http://192.168.2.194:3007/"

    const val CONNECT_TIMEOUT: Long = 10000
    const val READ_TIMEOUT: Long = 30000
    const val WRITE_TIMEOUT: Long = 30000


    const val SIGN_UP = "/api/v1/user/register"
    const val GET_USERS = "/api/v1/user/getusers"

}