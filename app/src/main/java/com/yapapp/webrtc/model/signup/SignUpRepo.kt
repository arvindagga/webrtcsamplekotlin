package com.yapapp.webrtc.model.signup

import com.rontaxi.retrofit.ApiService
import com.yapapp.webrtc.model.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SignUpRepo @Inject
constructor(val apiService: ApiService)
{

    suspend fun signUpUser(data: User) = apiService.signUpUser(data)
}
