package com.rontaxi.retrofit

import com.dizzipay.railsbank.data.remote.ApiConstants
import com.yapapp.webrtc.model.signup.LoginResponseModel
import com.yapapp.webrtc.model.User
import com.yapapp.webrtc.model.home.UserslistModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface ApiService {


    @POST(ApiConstants.SIGN_UP)
    suspend fun signUpUser(@Body data: User): Response<LoginResponseModel>


    @GET(ApiConstants.GET_USERS)
    suspend fun getUsers(): Response<UserslistModel>

}