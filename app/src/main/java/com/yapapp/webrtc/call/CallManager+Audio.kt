package com.yapapp.webrtc.call

import com.yapapp.webrtc.call.audio.AudioManager


/**
 * play tone according to call state
 */

fun CallManager.playAudio(callAudioType: AudioManager.CALL_AUDIO_TYPE){

    audioManager.setAudio(callAudioType)

}

/**
 * set audio mode according to call type
 */


fun CallManager.setAudioMode(){

    if(CallManager.callModel!!.callType==CallManager.CallType.VIDEO){
        audioManager.switchToSpeaker()
    }else{
        audioManager.switchToEarPieces()
    }
}


/**
 * stop tones
 */

fun CallManager.stopAudio(){

    audioManager.reset()
}
