package com.yapapp.webrtc.sockets

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import com.github.nkzawa.socketio.client.Ack
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.yapapp.webrtc.BuildConfig
import com.yapapp.webrtc.cache.clearAllData
import com.yapapp.webrtc.cache.getToken
import com.yapapp.webrtc.constants.SocketConstants
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject

class SocketManager(val socket: Socket, val context: Context) {


    enum class SOCKET_STATE {
        DISCONNECTED,
        CONNECTED,
        AUTHENTICATED

    }



    //Connect to the server and start listening the events from server
    fun connect() {


        if(!socket!!.connected() or (currentSocketState.value!=(SOCKET_STATE.AUTHENTICATED))){

                if (!getToken(context).isNullOrEmpty()) {

                    socket?.on("connect") {
                        Log.e("socketAuthenticate", "SOCKET_STATE.CONNECTED")
                        currentSocketState.postValue(SOCKET_STATE.CONNECTED)
                        authenticate()

                    }?.on("disconnect") {
                        Log.e("socketAuthenticate", "SOCKET_STATE.DISCONNECTED")
                        currentSocketState.postValue(SOCKET_STATE.DISCONNECTED)
                    }

                    socket?.connect()

                } else {
                    Log.e("socketAuthenticate", "No Access Token")
                }
            }
    }


    /*
   disconnect from server and stop listening to the events
    */

    fun disconnect() {

        socket?.disconnect()
        //socket?.off("connect",onConnect)


    }




    fun authenticate() {

        if (socket!!.connected()) {

            val jsonObject = JSONObject()
            jsonObject.put("token", "Bearer ${getToken(context)}")

            Log.e("socketAuthenticate", "check:  ${Gson().toJson(jsonObject)}")
            socket.emit(SocketConstants.AUTHENTICATE, jsonObject, object : Ack {
                override fun call(vararg args: Any?) {

                    val pair = isSocketResponseValid(JSONArray(args))

                    pair.first?.let {

                        if (isSocketResponseSuccess(it)) {
                            Log.e("socketAuthenticate", "SOCKET_STATE.AUTHENTICATED")
                            currentSocketState.postValue(SOCKET_STATE.AUTHENTICATED)
                        }
                    }

                    pair.second?.let {

                        val msg = handler.obtainMessage()
                        val bundle = Bundle()
                        bundle.putInt(SocketConstants.RESPONSE_CODE_KEY, it.code)
                        bundle.putString(SocketConstants.MESSAGE_TO_SHOW_KEY, it.message)
                        msg.data = bundle

                        handler.sendMessage(msg)

                        // handler.sendEmptyMessage(it.code)

                    }


                }
            })
        } else {
            connect()
        }

    }


    var handler= object: Handler(Looper.getMainLooper()){


        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)


            try {
                val bundle = msg!!.data


                Log.e("SocketManager", " $bundle")

                val message = bundle.getString(SocketConstants.MESSAGE_TO_SHOW_KEY)
                val code = bundle.getInt(SocketConstants.RESPONSE_CODE_KEY)

                when (code) {



                    // unAuthorize
                    401 -> {

                        Log.e("socketAuthenticate", "Logout 401 in socket")

                       // clearAllData(context)

                       // disconnect()
                        Toasty.error(context,message!!).show()


                    }

                    400->{

                        Toasty.error(context,message!!).show()

                    }

                }



                // Toasty.info(context!!, message).show()
            } catch (e: Exception) {

            }





        }
    }


}