package com.yapapp.webrtc.call.timer

import android.os.CountDownTimer
import android.util.Log

class TimerManager() {


    val TAG="TIMER_MANAGER"


    var connectingTimer: CountDownTimer?=null
    var outgoingTimer : CountDownTimer?=null
    var incomingTimer: CountDownTimer?=null

    lateinit var timerInterface: TimerInterface




    fun startConnectingTimer(){

        connectingTimer=null
        connectingTimer=object: CountDownTimer(15000, 1000){

            override fun onFinish() {
                timerInterface?.onConnectingTimeComplete()

            }

            override fun onTick(millisUntilFinished: Long) {
                Log.d(TAG,"Connecting timer $millisUntilFinished")
            }
        }.start()
    }


    fun startOutgoingTimer(){

        outgoingTimer=null
        outgoingTimer=object: CountDownTimer(30000, 1000){

            override fun onFinish() {
                timerInterface?.onOutGoingTimeComplete()

            }

            override fun onTick(millisUntilFinished: Long) {
            }
        }.start()
    }

    fun startIncomingTimer(){

        outgoingTimer=null
        outgoingTimer=object: CountDownTimer(40000, 1000){

            override fun onFinish() {
                timerInterface?.onInComingTimeComplete()

            }

            override fun onTick(millisUntilFinished: Long) {
            }
        }.start()
    }






    /**
     * cancel all kind of timers
     */

    fun clearAllTimers(){
        connectingTimer?.cancel()
        outgoingTimer?.cancel()
        incomingTimer?.cancel()
    }



}