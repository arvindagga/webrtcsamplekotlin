package com.yapapp.webrtc.constants

enum class CallType(val value: Int) {
    INCOMING(0),
    OUTGOING(1),
    ONGOING(2)
}