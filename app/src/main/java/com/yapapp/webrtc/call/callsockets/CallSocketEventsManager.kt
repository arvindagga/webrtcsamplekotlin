package com.yapapp.webrtc.call.callsockets

import android.os.Bundle
import com.dizzipay.railsbank.base.BaseError
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.Ack
import com.google.gson.Gson
import com.yapapp.webrtc.call.CallManager
import com.yapapp.webrtc.constants.SocketConstants
import com.yapapp.webrtc.model.User
import com.yapapp.webrtc.sockets.SocketManager
import com.yapapp.webrtc.sockets.isSocketResponseValid
import org.json.JSONArray
import org.json.JSONObject


/**
 * @author arvindaggarwal
 *
 *
 */

class CallSocketEventsManager(val socketManager: SocketManager) {





    lateinit var socketInterface: CallSocketInterface



    fun startListening(){
        listenCallRequest()
        listenReceiverConnectedToSocket()
        listenSDPOffer()
        listenSDPAnswer()
        listenICECandidates()
        listenCallOperations()
    }




    /**
     *  Call initiation(initial handshake request)
     *
     *  @param fromUserId is dialler id
     *  @param toUserId is receiver is
     *
     */

    fun createRequestToConnect(fromUser: User, toUser: User, callType: String){

        val request=getJsonObject(SocketConstants.REQUEST_TO_CONNECT,fromUser.id!!,toUser.id!!)



            val gson=Gson().toJson(fromUser)

            val data= JSONObject()
                data.put("diallerInfo",gson)
                data.put("callType", callType)
            request.put("data",data)

        socketManager.socket.emit(SocketConstants.REQUEST_TO_CONNECT, request, Ack{


            val pair= isSocketResponseValid(JSONArray(it))

                pair.second?.let { it ->

                    handleError(it)

                }


        })

    }


    /**
     *Call request reception on socket
     */

    private fun listenCallRequest(){

        socketManager.socket.on(SocketConstants.REQUEST_TO_CONNECT,Emitter.Listener{


            val pair= isSocketResponseValid(JSONArray(it))



            pair.first?.let {



                val str=it.getJSONObject("data").getJSONObject("data").getString("diallerInfo")


                val user=Gson().fromJson<User>(str,User::class.java)

                val callType=it.getJSONObject("data").getJSONObject("data").getString("callType")


                socketManager.handler.post(Runnable {
                    socketInterface.onCallRequestReceived(user, callType)
                })

            }


            pair.second?.let { it ->

                handleError(it)

            }



        })
    }


    /**
     * notify dialler on receiver connected to socket(complete handshake)
     *
     */

    fun receiverConnectedToSocket(fromUserId: String, toUserId: String,handshake: Boolean ){

        val request=getJsonObject(SocketConstants.RECEIVER_CONNECTED,fromUserId,toUserId)


            val json=JSONObject()
                json.put("handshake", handshake)

            request.put("data", json)


        socketManager.socket.emit(SocketConstants.RECEIVER_CONNECTED, request, Ack{

            val pair= isSocketResponseValid(JSONArray(it))

            pair.second?.let { it ->

                handleError(it)

            }

        })

    }


    /**
     * listen receiver connected to socket
     */

    private fun listenReceiverConnectedToSocket(){
        socketManager.socket.on(SocketConstants.RECEIVER_CONNECTED,Emitter.Listener{

            val pair= isSocketResponseValid(JSONArray(it))

            pair.second?.let { it ->

                handleError(it)

                return@Listener

            }


            pair.first?.let {

               val handshake= it.getJSONObject("data").getJSONObject("data").getBoolean("handshake")

                if(handshake){

                    socketManager.handler.post(Runnable {
                        socketInterface.onReceiverConnected()

                    })

                }else{

                    socketManager.handler.post {
                        socketInterface.onUserBusy()

                    }

                }
            }
        })

    }


    /**
     * SDPOffer sender
     */

    fun sendSDPOffer(fromUserId: String, toUserId: String, sdpType: String, description: String){

        val request=getJsonObject(SocketConstants.SDP_OFFER,fromUserId,toUserId)


            val json=JSONObject()
                json.put("sdpType", sdpType)
                json.put("description", description)

        request.put("data",json)

        socketManager.socket.emit(SocketConstants.SDP_OFFER, request, Ack{

            val pair= isSocketResponseValid(JSONArray(it))

            pair.second?.let { it ->

                handleError(it)

            }

        })

    }


    /**
     * listen SDP Offer
     */
    private fun listenSDPOffer(){

        socketManager.socket.on(SocketConstants.SDP_OFFER,Emitter.Listener{


            val jsonArray=JSONArray(it)
            val data=jsonArray.getJSONObject(0).getJSONObject("data")
            socketManager.handler.post(Runnable {


                socketInterface.onSDPOfferReceived(data.getString("sdpType"), data.getString("description"))


            })

        })
    }

    /**
     * SDPAnswer sender
     */

    fun sendSDPAnswer(fromUserId: String, toUserId: String, sdpType: String, description: String){

        val request=getJsonObject(SocketConstants.SDP_Answer,fromUserId,toUserId)

        val json=JSONObject()
        json.put("sdpType", sdpType)
        json.put("description", description)

        request.put("data",json)

        socketManager.socket.emit(SocketConstants.SDP_Answer, request, Ack{
            val pair= isSocketResponseValid(JSONArray(it))

            pair.second?.let { it ->

                handleError(it)

            }

        })

    }


    /**
     * listen SDP Offer
     */
    private fun listenSDPAnswer(){

        socketManager.socket.on(SocketConstants.SDP_Answer,Emitter.Listener{


            val jsonArray=JSONArray(it)
            val data=jsonArray.getJSONObject(0).getJSONObject("data")

            socketManager.handler.post(Runnable {


                socketInterface.onSDPAnswerReceived(data.getString("sdpType"), data.getString("description"))


            })


        })
    }


    /**
     * send ICE Candidates
     *
     */
    fun sendICECandidates(fromUserId: String, toUserId: String,  sdpMid: String,
                          sdpMLineIndex: Int,
                          sdp: String,
                          serverUrl: String){

        val request=getJsonObject(SocketConstants.ICE_CANDIDATES,fromUserId,toUserId)

        val data=JSONObject()
        data.put("sdpMid", sdpMid)
        data.put("sdpMLineIndex", sdpMLineIndex)
        data.put("sdp", sdp)
        data.put("serverUrl", serverUrl)

        request.put("data", data)

        socketManager.socket.emit(SocketConstants.ICE_CANDIDATES, request, Ack{
            val pair= isSocketResponseValid(JSONArray(it))

            pair.second?.let { it ->

                handleError(it)

            }

        })

    }

    /**
     *
     *receive ICE Candidates
     */

    private fun listenICECandidates(){

        socketManager.socket.on(SocketConstants.ICE_CANDIDATES,Emitter.Listener{

            val jsonArray=JSONArray(it)
            val data=jsonArray.getJSONObject(0).getJSONObject("data")

            socketManager.handler.post(Runnable {


                socketInterface.onICECandidateReceived(data.getString("sdpMid"), data.getInt("sdpMLineIndex"),data.getString("sdp"),data.getString("serverUrl"))


            })



        })
    }


    fun sendCallOperations(fromUserId: String, toUserId: String, callOperations: String){

        val request=getJsonObject(SocketConstants.CALL_OPERATIONS,fromUserId,toUserId)


            val data=JSONObject()
                data.put("callOperations", callOperations)


            request.put("data", data)


        socketManager.socket.emit(SocketConstants.CALL_OPERATIONS, request, Ack{
            val pair= isSocketResponseValid(JSONArray(it))

            pair.second?.let { it ->

                handleError(it)

            }

        })
    }


    private fun listenCallOperations(){


        socketManager.socket.on(SocketConstants.CALL_OPERATIONS, Emitter.Listener {


            val jsonArray=JSONArray(it)
            val data=jsonArray.getJSONObject(0).getJSONObject("data")

            socketManager.handler.post(Runnable {

                socketInterface.onCallOperationReceived(data.getString("callOperations"))
            })

        })

    }






    /**
     * generic format for json objects(all socket request will contain receiverId and senderId and data)
     *
     * @param requestType request type
     * @param fromUserId request initiator userId
     * @param toUserId reciever userId
     *
     */

    private fun getJsonObject(requestType: String, fromUserId: String, toUserId: String): JSONObject{

        return JSONObject().apply {

            this.put("requestType", requestType)
            this.put("fromUserId", fromUserId)
            this.put("toUserId", toUserId)
        }
    }



    interface CallSocketInterface{

        fun onCallRequestReceived(user: User, callType: String)

        fun onUserBusy()

        fun onReceiverConnected()

        fun onSDPOfferReceived(sdpType: String, description: String)

        fun onSDPAnswerReceived(sdpType: String, description: String)

        fun onICECandidateReceived(sdpMid: String, sdpMLineIndex: Int, sdp: String, serverUrl: String)

        fun onCallOperationReceived(callOperation: String)


    }



    private fun handleError(error: BaseError){

        val msg = socketManager.handler.obtainMessage()
        val bundle = Bundle()
        bundle.putInt(SocketConstants.RESPONSE_CODE_KEY, error.code)
        bundle.putString(SocketConstants.MESSAGE_TO_SHOW_KEY, error.message)
        msg.data = bundle

        socketManager.handler.sendMessage(msg)

    }




}