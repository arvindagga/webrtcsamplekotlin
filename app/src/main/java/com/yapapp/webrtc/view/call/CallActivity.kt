package com.yapapp.webrtc.view.call

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.di.viewmodelprovider.ViewModelFactory
import com.yapapp.webrtc.R
import com.yapapp.webrtc.base.BaseActivity
import com.yapapp.webrtc.call.CallManager
import com.yapapp.webrtc.call.addSurfaceViewRenderer
import com.yapapp.webrtc.model.User
import com.yapapp.webrtc.model.call.CallModel
import org.webrtc.EglBase
import org.webrtc.SurfaceViewRenderer
import javax.inject.Inject

class CallActivity : BaseActivity() {


    lateinit var callViewModel: CallViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var callManager: CallManager


    val localSurfaceRendrer: SurfaceViewRenderer by lazy {

        SurfaceViewRenderer(this)
    }




    val remoteSurfaceRendrer: SurfaceViewRenderer by lazy {

        SurfaceViewRenderer(this)
    }

    companion object {


        fun initVideoCall(context: Context, fromUser: User, toUser: User) {

            val intent = Intent(context, CallActivity::class.java)

            CallManager.callModel = CallModel(
                fromUser,
                toUser,
                CallManager.CallType.VIDEO,
                CallManager.CallState.OUT_GOING_VIDEO
            )
            context.startActivity(intent)
        }

        fun initAudioCall(context: Context, fromUser: User, toUser: User) {

            val intent = Intent(context, CallActivity::class.java)

            CallManager.callModel = CallModel(
                fromUser,
                toUser,
                CallManager.CallType.AUDIO,
                CallManager.CallState.OUT_GOING_AUDIO
            )
            context.startActivity(intent)
        }


        fun receiveVideoCall(context: Context, fromUser: User, toUser: User) {

            val intent = Intent(context, CallActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            CallManager.callModel = CallModel(
                fromUser,
                toUser,
                CallManager.CallType.VIDEO,
                CallManager.CallState.IN_COMING_VIDEO
            )
            context.startActivity(intent)
        }

        fun receiveAudioCall(context: Context, fromUser: User, toUser: User) {

            val intent = Intent(context, CallActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            CallManager.callModel = CallModel(
                fromUser,
                toUser,
                CallManager.CallType.AUDIO,
                CallManager.CallState.IN_COMING_AUDIO
            )
            context.startActivity(intent)
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call)
        showActionBar(false)
        configureViewModel()


        addObserver()


        /**
         * if this is video call only then add surfaceviewrenderers
         */
        if (CallManager.callModel?.callType == CallManager.CallType.VIDEO) {
            callViewModel.callManager.addSurfaceViewRenderer(localView = localSurfaceRendrer)

            callViewModel.callManager.addSurfaceViewRenderer(remoteView = remoteSurfaceRendrer)
        }


    }

    fun addObserver() {


        callViewModel.callStatusLiveData.observe(this, Observer {


            it?.let {

                when (it.callState) {

                    CallManager.CallState.ON_GOING_VIDEO -> {

                        // var id= this.findNavController(R.id.nav_host_fragment).()!!.getId()
                        val navHostFragment =
                            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)

                        var fragment =
                            navHostFragment?.getChildFragmentManager()?.getFragments()?.get(0);


                        //var fragment=getCurrentFragment(id)

                        if (fragment is CallFragment) {

                            fragment.activateCall()
                        }

                    }

                    CallManager.CallState.ON_GOING_AUDIO -> {
                        // var id= this.findNavController(R.id.nav_host_fragment).()!!.getId()
                        val navHostFragment =
                            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)

                        var fragment =
                            navHostFragment?.getChildFragmentManager()?.getFragments()?.get(0);


                        //var fragment=getCurrentFragment(id)

                        if (fragment is CallFragment) {

                            fragment.activateCall()
                        }
                    }


                    CallManager.CallState.NONE -> {

                        callViewModel.callManager.clearCallData()
                        this.finish()

                    }


                }


            }


        })

    }


    private fun configureViewModel() {

        callViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(CallViewModel::class.java)
    }


    override fun onBackPressed() {

        callViewModel.callManager.clearCallData()



        super.onBackPressed()
    }

}
