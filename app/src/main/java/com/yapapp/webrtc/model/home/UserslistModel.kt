package com.yapapp.webrtc.model.home

import com.dizzipay.railsbank.base.BaseResponseModel
import com.yapapp.webrtc.model.User

class UserslistModel : BaseResponseModel() {

    var data: ArrayList<User>? = null
}
