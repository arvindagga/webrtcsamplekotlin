package com.yapapp.webrtc.call

import android.content.Context
import com.yapapp.webrtc.cache.getUser
import com.yapapp.webrtc.call.audio.AudioManager
import com.yapapp.webrtc.call.callsockets.CallSocketEventsManager
import com.yapapp.webrtc.call.rtc.*
import com.yapapp.webrtc.call.timer.TimerInterface
import com.yapapp.webrtc.call.timer.TimerManager
import com.yapapp.webrtc.model.User
import com.yapapp.webrtc.model.call.CallModel
import com.yapapp.webrtc.view.call.CallActivity
import org.webrtc.SessionDescription
import kotlin.concurrent.timer

class CallManager(
    val context: Context, val rtcClient: RTCClient,
    val callSocketManager: CallSocketEventsManager,
    val audioManager: AudioManager,
    val timermanager: TimerManager
) : RTCInterface, CallSocketEventsManager.CallSocketInterface, TimerInterface {


    lateinit var callInterface: CallInterface

    companion object {

        var callModel: CallModel? = null

    }


    fun setObserver() {


        rtcClient.rtcInterface = this

        callSocketManager.socketInterface = this

        timermanager.timerInterface=this

    }


    /**
     * clear all data
     *
     * this fun called to end existing call
     *
     */

    fun clearCallData() {

        rtcClient.resetRTC()
        audioManager.resetMode()
        callModel = null
    }


    fun enableDisableVideo(enableVideo: Boolean, listener: RTCMediaStreamTrackListener) {
        rtcClient.enableDisableVideo(enableVideo, object : RTCMediaStreamTrackListener {
            override fun onSuccess(data: Any?) {
                listener.onSuccess(data)

                if (data is Boolean && data) {
                    postLocalVideoTrackStatus(enableVideo)
                }
            }

            override fun onError(error: String) {

                listener.onError(error)
            }
        })
    }

    fun postLocalVideoTrackStatus(enable: Boolean) {


        if (enable) {

            callSocketManager.sendCallOperations(
                callModel!!.callInitiator.id!!,
                callModel!!.callReceiver.id!!,
                CallOperations.VIDEO_ENABLED.value
            )
        } else {
            callSocketManager.sendCallOperations(
                callModel!!.callInitiator.id!!,
                callModel!!.callReceiver.id!!,
                CallOperations.VIDEO_DISABLED.value
            )
        }
    }


    private fun postLocalAudioTrackStatus(enable: Boolean) {


        if (enable) {

            callSocketManager.sendCallOperations(
                callModel!!.callInitiator.id!!,
                callModel!!.callReceiver.id!!,
                CallOperations.AUDIO_ENABLED.value
            )
        } else {
            callSocketManager.sendCallOperations(
                callModel!!.callInitiator.id!!,
                callModel!!.callReceiver.id!!,
                CallOperations.AUDIO_DISABLED.value
            )
        }


    }


    fun muteUnmuteAudio(enableAudio: Boolean, listener: RTCMediaStreamTrackListener) {
        rtcClient.muteUnmuteAudio(enableAudio, object : RTCMediaStreamTrackListener {
            override fun onSuccess(data: Any?) {
                listener.onSuccess(data)

                if (data is Boolean && data) {
                    postLocalAudioTrackStatus(enableAudio)
                }
            }

            override fun onError(error: String) {

                listener.onError(error)
            }
        })
    }


//================================================RTC interface methods==================================================

    override fun onICECandidateGenerated(
        sdpMid: String,
        sdpMLineIndex: Int,
        sdp: String,
        serverUrl: String
    ) {
        callSocketManager.sendICECandidates(
            callModel!!.callInitiator.id!!,
            callModel!!.callReceiver.id!!,
            sdpMid,
            sdpMLineIndex,
            sdp,
            serverUrl
        )
    }


    override fun onOfferCreated(sdp: SessionDescription) {

        callSocketManager.sendSDPOffer(
            callModel!!.callInitiator.id!!,
            callModel!!.callReceiver.id!!,
            sdp.type!!.toString(),
            sdp.description
        )

    }

    override fun onAnswerCreated(sdp: SessionDescription) {

        callSocketManager.sendSDPAnswer(
            callModel!!.callInitiator.id!!,
            callModel!!.callReceiver.id!!,
            sdp.type!!.toString(),
            sdp.description
        )

    }

    //======================================================= Call socket interface methods====================================


    override fun onCallRequestReceived(user: User, callType: String) {

        val currentUser = getUser(context)!!


        if (callModel != null) {

            sendBusyMessage(currentUser, user)

            return
        }

        playAudio(AudioManager.CALL_AUDIO_TYPE.IN_COMING)

        if (callType.equals(CallType.VIDEO.value)) {
            CallActivity.receiveVideoCall(context, currentUser, user)
        } else {
            CallActivity.receiveAudioCall(context, currentUser, user)
        }

    }

    override fun onUserBusy() {

        cancelAllTimer()
        stopAudio()
        callModel?.callState = CallState.NONE
        callInterface.onCallEnded()
        clearCallData()
    }

    override fun onReceiverConnected() {


        cancelAllTimer()
        startOutGoingTimer()

        playAudio(AudioManager.CALL_AUDIO_TYPE.OUT_GOING)

        rtcClient.createOffer()
    }

    override fun onSDPOfferReceived(sdpType: String, description: String) {
        rtcClient.setOffer(sdpType, description)
    }

    override fun onSDPAnswerReceived(sdpType: String, description: String) {

        rtcClient.setAnswer(sdpType, description)
    }

    override fun onICECandidateReceived(
        sdpMid: String,
        sdpMLineIndex: Int,
        sdp: String,
        serverUrl: String
    ) {

        rtcClient.processICECandidates(sdpMid, sdpMLineIndex, sdp, serverUrl)
    }

    override fun onCallOperationReceived(callOperation: String) {


        when (callOperation) {

            CallOperations.ACCEPT.value -> {



                cancelAllTimer()

                stopAudio()
                callInterface.onCallAccepted()

                if (callModel!!.callType == CallType.VIDEO) {
                    callModel!!.callState = CallState.ON_GOING_VIDEO
                } else {
                    callModel!!.callState = CallState.ON_GOING_AUDIO
                }

            }

            CallOperations.END_CALL.value -> {
                cancelAllTimer()
                stopAudio()
                callModel?.callState = CallState.NONE
                callInterface.onCallEnded()
                clearCallData()
            }


            CallOperations.VIDEO_ENABLED.value -> {
                callInterface.onRemoteVideoUpdated(true)
            }

            CallOperations.VIDEO_DISABLED.value -> {
                callInterface.onRemoteVideoUpdated(false)
            }

            CallOperations.AUDIO_ENABLED.value -> {
                callInterface.onRemoteAudioUpdated(true)
            }

            CallOperations.AUDIO_DISABLED.value -> {
                callInterface.onRemoteAudioUpdated(false)
            }
        }


    }


    //===================================================Timer interfaces ======================================================
    override fun onConnectingTimeComplete() {

        callModel?.callState = CallState.NONE
        callInterface.onCallEnded()
        endCall()



    }

    override fun onOutGoingTimeComplete() {

        callModel?.callState = CallState.NONE
        callInterface.onCallEnded()
        endCall()


    }

    override fun onInComingTimeComplete() {

        callModel?.callState = CallState.NONE
        callInterface.onCallEnded()
        endCall()
    }

    //==================================================== Enums ================================================================


    enum class CallType(val value: String) {
        VIDEO("VIDEO"),
        AUDIO("AUDIO")
    }

    enum class CallState {

        OUT_GOING_VIDEO,
        OUT_GOING_AUDIO,
        IN_COMING_VIDEO,
        IN_COMING_AUDIO,
        ON_GOING_VIDEO,
        ON_GOING_AUDIO,
        NONE
    }

    enum class CallOperations(val value: String) {

        ACCEPT("ACCEPT"),
        END_CALL("END_CALL"),
        CALL_ON_HOLD("CALL_ON_HOLD"),
        VIDEO_ENABLED("VIDEO_ENABLED"),
        VIDEO_DISABLED("VIDEO_DISABLED"),
        AUDIO_ENABLED("AUDIO_ENABLED"),
        AUDIO_DISABLED("AUDIO_DISABLED"),
    }

}