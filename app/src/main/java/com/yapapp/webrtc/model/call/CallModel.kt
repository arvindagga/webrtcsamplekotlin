package com.yapapp.webrtc.model.call

import com.yapapp.webrtc.call.CallManager
import com.yapapp.webrtc.model.User

data class CallModel(val callInitiator: User,
                     val callReceiver: User,
                     val callType: CallManager.CallType,
                     var callState: CallManager.CallState ){


}





