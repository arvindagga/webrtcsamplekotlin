package com.yapapp.webrtc.call


fun CallManager.startConnectingTimer(){

    timermanager.startConnectingTimer()
}


fun CallManager.startOutGoingTimer(){
    timermanager.startOutgoingTimer()
}

fun CallManager.startInComingTimer(){
    timermanager.startIncomingTimer()
}

fun CallManager.cancelAllTimer(){

    timermanager.clearAllTimers()
}

