package com.yapapp.webrtc.cache

import android.content.Context
import com.yapapp.webrtc.constants.CacheConstants
import com.yapapp.webrtc.model.User
import com.yapapp.webrtc.util.Prefs

fun getToken(context: Context): String? {
    return Prefs.with(context).getString(CacheConstants.USER_TOKEN, "")
}

fun saveToken(context: Context, token: String) {
    Prefs.with(context).save(CacheConstants.USER_TOKEN, token)
}


fun getUser(context: Context): User? {
    return Prefs.with(context).getObject(CacheConstants.USER_DATA, User::class.java)
}

fun saveUser(context: Context, user: User) {
    Prefs.with(context).save(CacheConstants.USER_DATA, user)
}
fun clearAllData(context: Context) {
    Prefs.with(context).removeAll()
}