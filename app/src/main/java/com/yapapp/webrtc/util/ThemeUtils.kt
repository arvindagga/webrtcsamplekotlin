package com.yapapp.webrtc.util

import androidx.annotation.IntDef
import androidx.annotation.StyleRes
import com.yapapp.webrtc.R
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy


object ThemeUtils {

    const val TYPE_NORMAL = 0
    const val TYPE_NO_ACTION_BAR = 1
    const val TYPE_TRANSPARENT_STATUS_BAR = 2

    @Retention(RetentionPolicy.SOURCE)
    @IntDef(TYPE_NORMAL, TYPE_NO_ACTION_BAR, TYPE_TRANSPARENT_STATUS_BAR)
    annotation class ThemeType

    @StyleRes
    fun themeFromId(themeId: String, @ThemeType type: Int): Int {
        when (type) {
            TYPE_NORMAL -> return themeNormalFromId(themeId)
            TYPE_NO_ACTION_BAR -> return themeNoActionBarFromId(themeId)
            TYPE_TRANSPARENT_STATUS_BAR -> return themeTransparentStatusBarFromId(themeId)
        }
        return themeNormalFromId(themeId)
    }

    @StyleRes
    fun themeNormalFromId(themeId: String): Int {
        when (themeId) {
            "light;pink" -> return R.style.AppTheme_Light_Pink
            "light;green" -> return R.style.AppTheme_Light_Green
            "dark;pink" -> return R.style.AppTheme_Dark_Pink
            "dark;green" -> return R.style.AppTheme_Dark_Green
            "amoled;pink" -> return R.style.AppTheme_AMOLED_Pink
            "amoled;green" -> return R.style.AppTheme_AMOLED_Green
        }
        return R.style.AppTheme_Light_Pink
    }

    @StyleRes
    fun themeNoActionBarFromId(themeId: String): Int {
        when (themeId) {
            "light;pink" -> return R.style.AppTheme_Light_Pink_NoActionBar
            "light;green" -> return R.style.AppTheme_Light_Green_NoActionBar
            "dark;pink" -> return R.style.AppTheme_Dark_Pink_NoActionBar
            "dark;green" -> return R.style.AppTheme_Dark_Green_NoActionBar
            "amoled;pink" -> return R.style.AppTheme_AMOLED_Pink_NoActionBar
            "amoled;green" -> return R.style.AppTheme_AMOLED_Green_NoActionBar
        }
        return R.style.AppTheme_Light_Pink_NoActionBar
    }

    @StyleRes
    fun themeTransparentStatusBarFromId(themeId: String): Int {
        when (themeId) {
            "light;pink" -> return R.style.AppTheme_Light_Pink_TransparentStatusBar
            "light;green" -> return R.style.AppTheme_Light_Green_TransparentStatusBar
            "dark;pink" -> return R.style.AppTheme_Dark_Pink_TransparentStatusBar
            "dark;green" -> return R.style.AppTheme_Dark_Green_TransparentStatusBar
            "amoled;pink" -> return R.style.AppTheme_AMOLED_Pink_TransparentStatusBar
            "amoled;green" -> return R.style.AppTheme_AMOLED_Green_TransparentStatusBar
        }
        return R.style.AppTheme_Light_Pink_TransparentStatusBar
    }
}